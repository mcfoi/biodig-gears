===========================================================
           WELCOME TO THE Biodig-Gears PROJECT
===========================================================
                        22 May 2014
						
This project aims at reproducing the activity of an industrial scale biodigester for the purpose of computing 
gas emissions and residual inactive masses that should be expected from a given input.

The key element is the BioSystem. This entity is instantiated by providing a couple of parameters: the 
volume of the pool (m^3) and the list of fluid withdrawals that are executed by the system maintainer. The 
volume should be expressed as a float (2550.0) - but is internally treated ad an integer (rounding occurs). 
The volumes list must take the form of a comma-separated-values file, with .csv extension (UTF8 encoding), 
and bearing a single column of 365 strings, one for each day of the year, (just one year can be simulated at 
a time!) that should be each one parsable to a double precision number. Either plain decimal format (e.g: 
125.56) or scientific notation (e.g: 8.34E-2) is  acceptable. Lines should be terminated by a 'line-feed' char.
An example of the content of this file could be:
====START OF FILE=====
40.09560000
22.83730000
39.56434000
39.03072000
...
====END OF FILE=====

The other key element is the InputTons. This element represent a single quantity of a specific material - 
hence on referred to as 'matrix' - measured in tons, having a specific 'age' and added to the BioSystem in a 
specific day of the year. Typical matrices are maize and rapeseed.

Internally the BioSystem makes use of the InputBag object to collect all InputTons of the same matrix so 
that calculations are executed separately.

The InputTons is also responsible to hold the results of all calculations performed on the represented 
amount of matrix to define its life in the BioSystem across the 365 simulated days. Such calculations are 
encapsulated in the classes implementing ICalculusLogic and the most complete - at time of writing - is 
FermentationAndGasLossCalculusLogic that also covers emitted gas.
In simple terms the logic starts from the quantity of the input expressed in tons.
A starting dilution factor for a unit of mass added to the system is computed from 1/SystemVolume.
This value is hence used in ( input-tons * daily-withdrawn-volume * dilution-factor ) to get how much of the 
input-tons will get out of the system the same day it was added. At the same time the initial input quantity 
is multiplied by a value coming from an experimental curve that represents the fermentation process of the 
considered matrix in a solution with 90% humidity. This allows to understand how much of the initial input-
tons will be get out of the system due to fermentation. This value plus the mass extracted from the system 
due to volume withdrawal are subtracted to the original mass to get the hoe much of it will be still in the 
system in the second day.
E.g.: A mass of 27 tons added to a system of 2550m^3 (initial dilution factor: 1/2550 = ~0.00039) from 
which 40.98m^3 are withdrawn the same day, will loose (0.00039 * 27 * 40.98) = 0.435 tons for subtraction 
and (27 * 0.229) = 6.18 tons for fermentation. Of the 27 tons added in day 1, we can consider just (27 - 
(0.435 + 6.18)) = 20.38 will be in the system at beginning of day 2.
Given that the total volume of the system is kept constant by other mass and water addition, we have to 
consider that the 20.38 tons will be further diluted. Since one ideal ton is diluted in the first day according 
to 1/SystemVolume, due to subtraction the remains of that ton in the second day will be ***********  (1.0 
- 0.00039) = 0.9996  ***********. This value is diluted once again in the total system volume so that the 
actual dilution factor for the second day is (0.9996 * 0.00039) = 0.0003898.
Consequently, for the second day, the 20.38 residual tons must be multiplied by this new dilution factor 
and for the volume withdrawn the second day to determine how much of the 20.38 tons will flow out of 
the system. Also, for computing the mass loss by fermentation, a new value from the experimental curve 
must be used since the matrix is not fresh any more but is 1-day old, hence its fermentation capacity has 
reduced.
Each quantity of matrix, lost for volume subtraction, is multiplied by a value coming from a second 
experimental curve expressing the gas produced for mass unit to determine HOW MUCH GAS WAS NOT 
PRODUCED BY THE SYSTEM. For this calculation the age of the subtracted masses is taken into account 
since a young matrix has an high gas production capacity while after 150 days in the system the 
experimental curve shows that produced gas is negligible and residual mass can be considered as not 
active.

This logic is computed for each InputTons added to the BioSystem hence the contribute of each addition in 
terms of gas produced and residual inactive residue can be assessed and exported in .csv and .xml files.

In the simpler case a BioSystem is fed with a list of matrix daily additions expressed in tons and provided in 
form of a column of doubles values inside a .csv file (UTF8 encoding), like the one for volume subtractions. 
In this scenario, all additions are "fresh" additions so all InputTons will have all 'age' equal to 1 while the 
'load day' will increase by one for each row (no two additions of the same matrix are possible for the same 
day - eventual 'morning' and 'evening' quantities from the system maintained log must be summed).
In this scenario, in the first day just a quantity of fresh matrix will be subtracted from the system by volume 
withdrawal. In the second day we will have two masses of two different ages getting out of the system: one 
as part of the fresh mass added in that second day and one as part of the remain of the first. The last day of 
the year we will have 365 quantities of the same matrix, all with different ages, getting out of the system. 
This is taken in consideration by the logic and explains why just not-produced GAS quantities can be 
summed by day: the addition of masses is meaningless as would group quantities with different chemical 
properties (apart from being of the same matrix).
