package eu.marcofoi.biodig.biodig_gears;

import java.util.HashMap;

public interface ICalculusLogic {

	/**
	 * Returns a map of arrays of doubles, containing 365 doubles each.</br>
	 * For the implementation of {@link FermentationCalculusLogic} three arrays are returned:
	 * residualMassYearArray, dailyOutputMassYearArray, tss_lostYearArray</br> 
	 * For the implementation of {@link FermentationAndGasLossCalculusLogic} four arrays are returned:
	 * residualMassYearArray, dailyOutputMassYearArray, tss_lostYearArray, GAS_LOST_YEAR_ARRAY
	 * @param startVolume
	 * @return a map of arrays using as keys the values of the Enum associated with
	 * the {@link ICalculusLogic#getCalculatedArrays()} method.
	 *         
	 */
	HashMap<Enum<?>, double[]> calculateFermentationArrays(InputTons inputTons);
	
	HashMap<Enum<?>, double[]> calculateFermentationArrays(InputTons inputTons, double startTons, int startAge);
	
	ICalculatedArrays[] getCalculatedArrays();

	Class<? extends Enum> getCalculatedArraysEnum();
	
	
}