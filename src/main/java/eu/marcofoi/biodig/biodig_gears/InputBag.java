package eu.marcofoi.biodig.biodig_gears;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import eu.marcofoi.biodig.biodig_gears.feeds.Feed;
import eu.marcofoi.biodig.biodig_gears.io.IOutputWriterBehavior;
import eu.marcofoi.biodig.biodig_gears.io.OutputParser;
import eu.marcofoi.biodig.biodig_gears.utils.Utils;

/**
 * An InputBag is used to store, within the same BioSystem, all feeds (InputTons) of the same type (e.g.:all maize
 * feeds). Each entry in the Bag has the key given by the load day of the feed itself so to easily access the feeds
 * added to the BioSystem in a given day. The key is equal to the inputTons.getLoadDay() response. Be ware of this when
 * retrieving data: key set starts from 1 (not from 0!!).</br>
 * Note also that an InputBag stores a List of InputTons for EACH load day:
 * this is a key requirement and was added to to allow this data structure to serve multi-pool {@link BioSystem}s
 * where for each day multiple feeds pass form one pool to the subsequent and each of them has a different age hence
 * requires a different management. (E.g.: the experimental curves of gas emissions allow to estimate how much gas can
 * be
 * produced by a feed but the computation requires the age to be carried out)</br>
 * The chosen logic is that of keeping all feeds of same kind in a single InputBag and from here to allow their
 * management and export using the InputBag API.
 * 
 * @author Marco Foi marco.foi@unimi.it
 * @date 21/02/2014
 */
public class InputBag extends HashMap<Integer, List<InputTons>> {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	private int					count				= 0;
	private Feed				feedType;
	private ICalculusLogic		calculusLogic;
	private BioSystem			bioSystem;

	// private Map<Integer, InputTons> inputHashMap = new HashMap<>();

	/**
	 * An InputBag is used to store, within the same BioSystem, all feeds
	 * (InputTons) of the same type (e.g.:all maize feeds). Each entry in the
	 * Bag has key given by the load day of the feed itself so to easily access
	 * the feed added to the BioSystem in a given day. The key is equal to the
	 * inputTons.getLoadDay() response. Be ware of this when retrieving data:
	 * key set starts from 1 (not from 0!!) Note also that NO two InputTons can
	 * share the same load day (key of Set) so any InputMe with same LoadDay as
	 * one previously loaded, replaces the older one. No constraint is made on
	 * duplicate values as would
	 * 
	 * @param bioSystem
	 * @param feedType
	 *            the type of the bag, coming by FeedType.toString() method and
	 *            stored on construction
	 */
	public InputBag(BioSystem bioSystem, Feed bagType, ICalculusLogic calculusLogic) {
		this.bioSystem = bioSystem;
		this.feedType = bagType;
		this.calculusLogic = calculusLogic;
	}

	/**
	 * Returns the type of bag so allowing to understand what kind of feed it
	 * can accept. An InputBag can contain just one kind of {@link Feed}.
	 * 
	 * @return
	 */
	public Feed getBagType() {
		return this.feedType;
	}

	/**
	 * This method has been overridden to throw an UnsupportedOperationException to force using {@link #put(InputTons)}.
	 * The reasons are that this method is not synchronized and it would require the client class to pass also the key
	 * to the map, whereas it can be retrieved from the value
	 */
	@Override
	public List<InputTons> put(Integer loadDay, List<InputTons> inputTonsList) {
		throw new UnsupportedOperationException();
	}

	/**
	 * A utility method to wrap the overridden 'put' method and saving from
	 * passing the key during massive input load. This methods also sets the {@link ICalculusLogic} of the InputTons.
	 * Note the this method is synchronized so that multiple threads cannot put data inside the data structure at the
	 * same time.
	 * 
	 * @param the
	 *            InputTons to be added to the InputBag
	 */
	public synchronized void put(InputTons input) {

		if (input.getFeedtype().equals(this.feedType)) {

			// Retrieve the list of inputs for the same day as the to-be-added
			// input
			List<InputTons> l = this.get(input.getLoadDay());
			// If the list is null - no existing input for that loadDay in the
			// bag -
			// then create the list
			if (l == null) {
				this.putCustom(input.getLoadDay(), l = new ArrayList<InputTons>());
			}

			// Very important: set calculus logic!
			// It is NOT set at InputTons construction!!!
			input.setCalculusLogic(calculusLogic);

			// Add the input to the list, increasing internal counter
			count++;
			l.add(input);
		} else {
			throw new UnsupportedOperationException();
		}
	}

	@Override
	public List<InputTons> remove(Object key) {
		count--;
		return super.remove(key);
	}

	/**
	 * This private method is internally used by the custom synchronized method {@link InputBag#put(InputTons)} to
	 * update the HashMap<,> that is the parent of the InputBag class.
	 * 
	 * @param loadDay
	 * @param inputTonsList
	 * @return List<InputTons> the previous content of the list
	 */
	private List<InputTons> putCustom(Integer loadDay, List<InputTons> inputTonsList) {
		// Call the parent ordinary {@link HashMap#put(key, value)} method
		return super.put(loadDay, inputTonsList);
	}

	/**
	 * Return the number of InputTons added to the InputBag.
	 * Does not compute the value but returns an internl counter.
	 * 
	 * @return
	 */
	public int countInputTons() {
		return count;
	}

	/**
	 * This method allows clients of InputBag (e.g.: {@link BioSystem}) to ask for the
	 * computation of all data of all {@link InputTons} available in the InputBag. The
	 * actual execution (and storage of results) is delegated to each InputTons
	 */
	public void processInputs() {
		// Retrieve the list of days in which the system was added with some
		// InputTons (in some days the BioSystem might not have been added with new feeds)
		Set<Integer> loadDaySet = this.keySet();
		// Loop in the days (NOTE that Set is NOT guaranteed to be ORDERED!!)
		for (Integer loadDay : loadDaySet) {

			// IMPORTANT NOTE: InputTons CAN accept multiple loads of the same Feed for the same day!!
			// This was introduced to allow feeding in an InputBag contributes of different ages, as those that each
			// day come from a previous stage of a multi-pool BioSystem.

			// Get the list of InputTons loaded a single day (multiple addition of InputBags of same feed the same day)
			List<InputTons> l = this.get(loadDay);
			// Execute the logic of each input
			for (InputTons input : l) {

				// ###############################
				// TODO: this can be parallelized
				input.executeCalcLogic();
				// ###############################

			}
		}

	}

	/**
	 * This method causes the ............... to be exported to a file of type
	 * descending from the passed IOutputWriterBehavior. The file name will get
	 * a prefix from the Feed type of the InputBag under processing so a
	 * InputBag of type "maize" using a XLSXOutputWriterSUMandSINGLEInputPerDayBehaviour will output a
	 * file named "maize.xlsx"
	 * 
	 * @param writerBehavior
	 */
	/*
	 * public void exportComputation(IOutputWriterBehavior writerBehavior) {
	 * double[][] yearMatrix = getTSSlossYearMatrix(); String outputFilePrefix =
	 * feedType.toString(); String fileExtension =
	 * writerBehavior.getOutputFileExtension();
	 * OutputParser op = new OutputParser(writerBehavior);
	 * op.saveYearMatrixToFile(yearMatrix, "./" + outputFilePrefix + "_" +
	 * Utils.getDateAsString() + fileExtension);
	 * }
	 */

	/*
	 * private double[][] getTSSlossYearMatrix() { Set<Integer> loadDaySet =
	 * this.keySet(); InputTons input;
	 * // External loop: progress through the days of the year double[][]
	 * yearMatrix = new
	 * double[BioSystem.computationLengthInDays][BioSystem.computationLengthInDays
	 * ]; for (int day_of_year = 1; day_of_year <=
	 * BioSystem.computationLengthInDays; day_of_year++) { // Internal loop:
	 * cycle through all the inputs of the bag and // retrieve from them the
	 * sought value double[] dayRow = new
	 * double[BioSystem.computationLengthInDays]; // Scan through the loadDays:
	 * note that 'loadDaySet' being a Set is // not strictly sequential for
	 * (Integer loadDay : loadDaySet) { input = this.get(loadDay);
	 * dayRow[loadDay - 1] = input.getTSSloss(day_of_year); }
	 * yearMatrix[day_of_year - 1] = dayRow; }
	 * return yearMatrix; }
	 */

	/**
	 * This method causes all data computed for all the {@link InputTons} contained in the InputBag to be returned as a
	 * matrix containing in first index one of the n arrays of data, as returned by
	 * {@link ICalculatedArrays#getCalculatedArrays()}.</br>
	 * The other two indexes contain the rows of the data matrix (corresponding to a day_of_year) and the columns (the
	 * loadDay).
	 * 
	 * @return the exported matrix
	 */
	public double[][][][] getComputation() {

		ICalculatedArrays[] calcArrays = this.calculusLogic.getCalculatedArrays();

		double[][][][] allArraysYearMatrix = new double[calcArrays.length][][][];

		int i = 0;
		for (ICalculatedArrays e : calcArrays) {

			double[][][] yearMatrix = getCaclulatedArrayYearMatrix((Enum<?>) e);

			allArraysYearMatrix[i] = yearMatrix;
			i++;
		}

		return allArraysYearMatrix;
	}

	/**
	 * This method causes all data computed for all the {@link InputTons} contained in the InputBag to be returned as a
	 * matrix containing at index 0 the requested array of data.</br>
	 * The other two indexes contain the rows of the data matrix (corresponding to a day_of_year), the columns (the
	 * loadDay) and the InputTons available (An InputBag may contain more than an InputTon for a single day).
	 * 
	 * @return the exported matrix in the form [0]
	 */
	public double[][][][] getComputation(Enum arrayToReturn) {

		ICalculatedArrays[] calcArrays = this.calculusLogic.getCalculatedArrays();

		double[][][][] allArraysYearMatrix = new double[1][][][];

		int i = 0;
		for (ICalculatedArrays e : calcArrays) {

			if (e.equals(arrayToReturn)) {

				double[][][] yearMatrix = getCaclulatedArrayYearMatrix((Enum<?>) e);

				allArraysYearMatrix[i] = yearMatrix;
				i++;

			}
		}

		return allArraysYearMatrix;
	}

	/**
	 * This method causes all data computed for all the {@link InputTons} contained in the InputBag to be exported to a
	 * file of type depending from the passed IOutputWriterBehavior.</br>
	 * The file name will get a prefix from the Feed type of the InputBag under processing so a InputBag of type
	 * "MAIZE" using a XLSXOutputWriterSUMandSINGLEInputPerDayBehaviour will output a file named
	 * "MAIZE_ARRAY_OF_DATA_EXPORTED_yyyyMMdd-HHmmss.xlsx"
	 * 
	 * @param writerBehavior
	 * @return the exported matrix
	 */
	public double[][][][] exportComputation(IOutputWriterBehavior writerBehavior) {

		ICalculatedArrays[] calcArrays = this.calculusLogic.getCalculatedArrays();

		double[][][][] allArraysYearMatrix = new double[calcArrays.length][][][];

		int i = 0;
		for (ICalculatedArrays e : calcArrays) {
			// The matrix is [day_of_year][loadDay][ICalculatedArrays*]
			// *ICalculatedArrays: 0= ICalculatedArrays[0]; 1= ICalculatedArrays[1]; ..
			double[][][] yearMatrix = getCaclulatedArrayYearMatrix((Enum<?>) e);

			String outputFilePrefix = bioSystem.getName() + "_" + feedType.toString();

			String fileExtension = writerBehavior.getOutputFileExtension();

			OutputParser op = new OutputParser(writerBehavior);

			op.saveYearMatrixToFile(yearMatrix, FilenameBuilder.build(outputFilePrefix, e.toString(), writerBehavior.getSuffix(), fileExtension));

			allArraysYearMatrix[i] = yearMatrix;
			i++;
		}

		return allArraysYearMatrix;

	}

	/**
	 * This method causes all data computed for all the {@link InputTons} contained in the InputBag to be exported to a
	 * file of type depending from the passed IOutputWriterBehavior.</br>
	 * The file name will get a prefix from the Feed type of the InputBag under processing so a InputBag of type
	 * "MAIZE" using a XLSXOutputWriterSUMandSINGLEInputPerDayBehaviour will output a file named
	 * "MAIZE_ARRAY_OF_DATA_EXPORTED_yyyyMMdd-HHmmss.xlsx"
	 * 
	 * @param ICalculatedArrays
	 *            The name of the array to export
	 * @param writerBehavior
	 *            The IOutputWriterBehaviour to use for writing output file.
	 * @return the exported matrix
	 */
	public double[][][][] exportComputation(ICalculatedArrays toBeExportedArray, IOutputWriterBehavior writerBehavior) {

		// ICalculatedArrays[] calcArrays = this.calculusLogic.getCalculatedArrays();

		double[][][][] allArraysYearMatrix = new double[1][][][];

		int i = 0;
		// for (ICalculatedArrays e : calcArrays) {
		// The matrix is [day_of_year][loadDay][value_of_toBeExportedArray]
		double[][][] yearMatrix = getCaclulatedArrayYearMatrix((Enum<?>) toBeExportedArray);

		System.out.println(yearMatrix.length);
		System.out.println(yearMatrix[0].length);
		System.out.println(yearMatrix[0][0].length);

		String outputFilePrefix = bioSystem.getName() + "_" + feedType.toString();

		String fileExtension = writerBehavior.getOutputFileExtension();

		OutputParser op = new OutputParser(writerBehavior);

		op.saveYearMatrixToFile(yearMatrix, FilenameBuilder.build(outputFilePrefix, toBeExportedArray.toString(), writerBehavior.getSuffix(), fileExtension));

		allArraysYearMatrix[i] = yearMatrix;
		i++;
		// }

		return allArraysYearMatrix;

	}

	/**
	 * Private method for composing the data from all {@link InputTons} into one
	 * single matrix.
	 * 
	 * @param e
	 *            The Enum that selects what array among those stored in a
	 *            single {@link InputTons} to pick among those computed by the {@link ICalculusLogic} instance.
	 * @return double[][][] A matrix af all data for all days of the year for all {@link InputTons} of the InputBag
	 */
	private double[][][] getCaclulatedArrayYearMatrix(Enum<?> e) {
		Set<Integer> loadDaySet = this.keySet();

		// External loop: progress through the days of the year
		double[][][] yearMatrix = new double[BioSystem.computationLengthInDays][BioSystem.computationLengthInDays][];
		for (int day_of_year = 1; day_of_year <= BioSystem.computationLengthInDays; day_of_year++) {
			// Internal loop: cycle through all the inputs of the bag and
			// retrieve from them the sought value
			double[][] dayRow = new double[BioSystem.computationLengthInDays][];
			// Scan through the loadDays: note that 'loadDaySet' being a Set is
			// not strictly sequential
			for (Integer loadDay : loadDaySet) {
				// Extract the list of all inputs added on that load day
				List<InputTons> l = this.get(loadDay);
				// Set the array lenght accordingly
				double[] inputsList = new double[l.size()];
				// Cycle in the inputs and extract the sought value from the desired array
				int inputNumber = 0;
				for (InputTons input : l) {
					double d = input.getValueFromCalculatedArray(day_of_year, e);
					// This picks the value from the array defined by the Enum e.
					inputsList[inputNumber] = d;
					inputNumber++;
				}
				dayRow[loadDay - 1] = inputsList;
			}
			yearMatrix[day_of_year - 1] = dayRow;
		}
		return yearMatrix;
	}

	/**
	 * This Class is used to build the String that is sent by the exportComputation() methods to determine the ultimate
	 * name of the exported file.
	 * 
	 * @author Marco Foi
	 * @date 13/05/2014
	 */
	private static class FilenameBuilder {

		private FilenameBuilder() {
		}

		public static String build(String outputFilePrefix, String arrayname, String suffix, String fileExtension) {

			String relativeFilename = "./" + BioSystemCLI.OUTPUT_FOLDER + outputFilePrefix + "_" + arrayname + "_" + suffix + "_" + Utils.getDateAsString() + fileExtension;
			return relativeFilename;
		}

	}

}
