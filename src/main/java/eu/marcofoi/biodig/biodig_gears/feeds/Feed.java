package eu.marcofoi.biodig.biodig_gears.feeds;

import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.InvalidParameterException;

import eu.marcofoi.biodig.biodig_gears.BioSystem;
import eu.marcofoi.biodig.biodig_gears.BioSystemCLI;
import eu.marcofoi.biodig.biodig_gears.InputBag;
import eu.marcofoi.biodig.biodig_gears.io.IOutputWriterBehavior;
import eu.marcofoi.biodig.biodig_gears.io.InputParser;
import eu.marcofoi.biodig.biodig_gears.utils.Utils;

public abstract class Feed {

	public static final String	FEED_FILENAME_PREFIX					= "FEED";
	public static final String	TSS_FILENAME_PREFIX						= "TSS";
	public static final String	DAILY_LOSS_TO_GAS_FILENAME_PREFIX		= "DAILY_MASS_to_GAS";
	public static final String	SPACER									= "_";

	protected String			feedName;
	protected String			lossFileName;
	protected String			gasProductionFileName;
	private double[]			t_ss_lost_for_ttq						= new double[BioSystem.computationLengthInDays];
	private double[]			experimentalCurveOfDailyGassProduction	= new double[BioSystem.computationLengthInDays];

	/**
	 * Constructor of a Feed (IT: 'matrice'. e.g.: 'mais', 'loietto',etc)
	 * This implementation is the most limited as allows the creation of a feed just providing the volume-to-mass
	 * conversion curve. This curve is needed to convert data to Mass starting from the information for
	 * the whole BioSysyem that is computed using Volumes. In other terms this take into account the humidity of the
	 * feed to allow deriving from the Volumes extracted from the BioSustem the Masses of feed that is actually
	 * involved.
	 * 
	 * @param MAISFEEDNAME
	 * @param double[] Target array fileTSS_LOST_on_TTQ
	 */
	public Feed(String feedName, URL fileTSS_LOST_on_TTQ) {
		this.feedName = feedName;
		try {
			this.lossFileName = fileTSS_LOST_on_TTQ.toURI().toString();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		loadFileIntoArray(fileTSS_LOST_on_TTQ, this.t_ss_lost_for_ttq);
	};

	/**
	 * Constructor of a Feed (IT: 'matrice'. e.g.: 'mais', 'loietto',etc)
	 * This implementation allows the creation of a feed providing both the volume-to-mass conversion curve and the
	 * mass-to-produced_gas curve for the specific feed.
	 * <ul>
	 * <li>The first curve is needed to convert data to Mass starting from the information for the whole BioSysyem that
	 * is computed using Volumes (constraint impsed by the fact that the BioSystem is sized by a volume and daily
	 * subtractions from the volumes are measured in Volumes). In other terms this first curve takes into account the
	 * humidity of the feed to allow deriving from the Volumes extracted from the BioSustem the Masses of feed that is
	 * actually removed.</li>
	 * <li>The second curve is required to convert the masses lost by volume subtraction into volumes of gas that was
	 * not produced by the BioSystem. This is particularly important in multi-stage (IT: 'multi-vasca') BioSystems as a
	 * unit mass of feed that moves from one stage (pool) to another keeps aging and this must be taken into account
	 * when considering how much gas that unit might have still produced if it was not subtracted from the system as
	 * part of the ordinary daily process of volume subtraction. In other terms just the first pool of the BioSystem
	 * only receives fresh feeds. All other receive each day a number of feeds of all different ages available in the
	 * preceding pool: each feed will produce gas according to its age. Here the need of converting mass loss (having a
	 * mixture of feeds of different ages, into a daily volume of gas produced)</li>
	 * </ul>
	 * 
	 * @param String
	 *            MAISFEEDNAME
	 * @param URL
	 *            fileTSS_LOST_on_TTQ
	 * @param URL
	 *            fileExperimentalCureveOfGasProductionPerAgingMassUnit
	 */
	public Feed(String feedName, URL fileTSS_LOST_on_TTQ, URL fileExperimentalCureveOfGasProductionPerAgingMassUnit) {
		this.feedName = feedName;
		try {
			this.lossFileName = fileTSS_LOST_on_TTQ.toURI().toString();
			this.gasProductionFileName = fileExperimentalCureveOfGasProductionPerAgingMassUnit.toURI().toString();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		loadFileIntoArray(fileTSS_LOST_on_TTQ, this.t_ss_lost_for_ttq);
		loadFileIntoArray(fileExperimentalCureveOfGasProductionPerAgingMassUnit, this.experimentalCurveOfDailyGassProduction);
	};

	/**
	 * Constructor of a Feed (IT: 'matrice'. e.g.: 'mais', 'loietto',etc)
	 * This implementation allows the creation of a feed providing both the volume-to-mass conversion curve and the
	 * mass-to-produced_gas curve for the specific feed (in the form of InputStream - as required for accessing
	 * resources compiled into .JAR files).
	 * <ul>
	 * <li>The first curve is needed to convert data to Mass starting from the information for the whole BioSysyem that
	 * is computed using Volumes (constraint impsed by the fact that the BioSystem is sized by a volume and daily
	 * subtractions from the volumes are measured in Volumes). In other terms this first curve takes into account the
	 * humidity of the feed to allow deriving from the Volumes extracted from the BioSustem the Masses of feed that is
	 * actually removed.</li>
	 * <li>The second curve is required to convert the masses lost by volume subtraction into volumes of gas that was
	 * not produced by the BioSystem. This is particularly important in multi-stage (IT: 'multi-vasca') BioSystems as a
	 * unit mass of feed that moves from one stage (pool) to another keeps aging and this must be taken into account
	 * when considering how much gas that unit might have still produced if it was not subtracted from the system as
	 * part of the ordinary daily process of volume subtraction. In other terms just the first pool of the BioSystem
	 * only receives fresh feeds. All other receive each day a number of feeds of all different ages available in the
	 * preceding pool: each feed will produce gas according to its age. Here the need of converting mass loss (having a
	 * mixture of feeds of different ages, into a daily volume of gas produced)</li>
	 * </ul>
	 * 
	 * @param String
	 *            MAISFEEDNAME
	 * @param InputStream
	 *            inputStreamTSS_LOST_on_TTQ
	 * @param InputStream
	 *            inputStreamExperimentalCureveOfGasProductionPerAgingMassUnit
	 */
	public Feed(String feedName, InputStream inputStreamTSS_LOST_on_TTQ, InputStream inputStreamExperimentalCureveOfGasProductionPerAgingMassUnit) {
		this.feedName = feedName;

		this.lossFileName = inputStreamTSS_LOST_on_TTQ.toString();
		this.gasProductionFileName = inputStreamExperimentalCureveOfGasProductionPerAgingMassUnit.toString();

		loadInputStreamIntoArray(inputStreamTSS_LOST_on_TTQ, this.t_ss_lost_for_ttq);
		loadInputStreamIntoArray(inputStreamExperimentalCureveOfGasProductionPerAgingMassUnit, this.experimentalCurveOfDailyGassProduction);
	};

	/**
	 * Constructor of a Feed (IT: 'matrice'. e.g.: 'mais', 'loietto',etc)
	 * This implementation allows the creation of a feed providing both the volume-to-mass conversion curve and the
	 * mass-to-produced_gas curve for the specific feed.
	 * <ul>
	 * <li>The first curve is needed to convert data to Mass starting from the information for the whole BioSysyem that
	 * is computed using Volumes (constraint impsed by the fact that the BioSystem is sized by a volume and daily
	 * subtractions from the volumes are measured in Volumes). In other terms this first curve takes into account the
	 * humidity of the feed to allow deriving from the Volumes extracted from the BioSustem the Masses of feed that is
	 * actually removed.</li>
	 * <li>The second curve is required to convert the masses lost by volume subtraction into volumes of gas that was
	 * not produced by the BioSystem. This is particularly important in multi-stage (IT: 'multi-vasca') BioSystems as a
	 * unit mass of feed that moves from one stage (pool) to another keeps aging and this must be taken into account
	 * when considering how much gas that unit might have still produced if it was not subtracted from the system as
	 * part of the ordinary daily process of volume subtraction. In other terms just the first pool of the BioSystem
	 * only receives fresh feeds. All other receive each day a number of feeds of all different ages available in the
	 * preceding pool: each feed will produce gas according to its age. Here the need of converting mass loss (having a
	 * mixture of feeds of different ages, into a daily volume of gas produced)</li>
	 * </ul>
	 * 
	 * @param String
	 *            MAISFEEDNAME
	 * @param String
	 *            fileTSS_LOST_on_TTQ
	 * @param String
	 *            fileExperimentalCureveOfGasProductionPerAgingMassUnit
	 */
	public Feed(String feedName, String lossfileName, String gasProductionfileName) {
		this(feedName, Feed.class.getResource(lossfileName), Feed.class.getResource(gasProductionfileName));
		this.lossFileName = lossfileName;
		this.gasProductionFileName = gasProductionfileName;
	}

	/**
	 * This method is used by Feed constructor for storing data in an double[] array object.
	 * It relies on a couple of methods that check wheter the provided day of year is within allowed range (1:365)
	 * 
	 * @param InputStream
	 *            InputStream of the file to be loaded into a Feed parameter as a double[] array
	 */
	private void loadInputStreamIntoArray(InputStream fileInputStream, double[] targetArray) {

		double[] doubleArray = InputParser.parseInputStreamToDoubleArray(fileInputStream);

		if (targetArray == this.t_ss_lost_for_ttq) {
			for (int i = 0; i < doubleArray.length; i++) {
				setTSSLostOnTTQ_on_day_of_year(i + 1, doubleArray[i]);
			}
		} else if (targetArray == this.experimentalCurveOfDailyGassProduction) {
			for (int i = 0; i < doubleArray.length; i++) {
				setGASProduction_on_day_of_year(i + 1, doubleArray[i]);
			}
		}

	}

	/**
	 * This method is used by Feed constructor for storing data in an double[] array object
	 * It relies on a couple of methods that check wheter the provided day of year is within allowed range (1:365)
	 * 
	 * @param URL
	 *            URL of file to be loaded into a Feed parameter as a double[] array.
	 */
	private void loadFileIntoArray(URL fileURL, double[] targetArray) {

		double[] doubleArray = InputParser.parseSingleColumnCSVFileToDoubleArray(fileURL);

		if (targetArray == this.t_ss_lost_for_ttq) {
			for (int i = 0; i < doubleArray.length; i++) {
				setTSSLostOnTTQ_on_day_of_year(i + 1, doubleArray[i]);
			}
		} else if (targetArray == this.experimentalCurveOfDailyGassProduction) {
			for (int i = 0; i < doubleArray.length; i++) {
				setGASProduction_on_day_of_year(i + 1, doubleArray[i]);
			}
		}

	}

	/**
	 * Loads into array checking the index is a vaild day of the year.
	 * 
	 * @param day_of_year
	 *            The day of the year: allowed range from 1 to 365
	 * @param lostMass
	 *            The lost mass cannot be negative.
	 */
	private void setTSSLostOnTTQ_on_day_of_year(int day_of_year, double lostMass) {
		if (day_of_year < 1 || day_of_year > 365 || lostMass < 0) {
			throw new UnsupportedOperationException();
		} else {
			this.t_ss_lost_for_ttq[day_of_year - 1] = lostMass;
		}
	}

	/**
	 * Loads into array checking the index is a vaild day of the year.
	 * 
	 * @param day_of_year
	 *            The day of the year: allowed range from 1 to 365
	 * @param dailyGasProduction
	 *            The lost mass cannot be negative.
	 */
	private void setGASProduction_on_day_of_year(int day_of_year, double gasProduced) {
		if (day_of_year < 1 || day_of_year > 365 || gasProduced < 0) {
			throw new UnsupportedOperationException();
		} else {
			this.experimentalCurveOfDailyGassProduction[day_of_year - 1] = gasProduced;
		}
	}
	
	/**
	 * Returns the name of the feed as set in the subclass. This name is used as
	 * a prefix for the file name when a matrix of data from an {@link InputBag} is dumped to a file using a
	 * {@link IOutputWriterBehavior}.
	 */
	public String getFeedName(){
		return feedName;
	}

	/**
	 * Returns the name of the feed as set in the subclass. This name is used as
	 * a prefix for the file name when a matrix of data from an {@link InputBag} is dumped to a file using a
	 * {@link IOutputWriterBehavior}.
	 */
	@Override
	public String toString() {
		return feedName;
	}

	/**
	 * Returns the whole array of all empirically calculated losses for the
	 * specific feed type
	 * 
	 * @return double[]
	 */
	public double[] getTSSLoss_on_TTQ_Array() {
		return this.t_ss_lost_for_ttq;
	}

	/**
	 * Returns the value of Mass lost of a single unit mass at the given day of the year, caused by the interaction of
	 * the dilution mechanism and the daily subtraction of volumes from the host BioSystem.
	 * 
	 * @param day_of_year
	 *            The day of the year: ranges from 1 to 365
	 */
	public double getTSSLostOnTTQ_on_day_of_year(int day_of_year) {
		return this.t_ss_lost_for_ttq[day_of_year - 1];
	}

	/**
	 * Returns the whole array of all empirically calculated daily gas volumes, produced by a unit mass of the specific
	 * feed type in its life inside the BioSystem.
	 * 
	 * @return double[]
	 */
	public double[] getGAS_Loss_Array() {
		return this.experimentalCurveOfDailyGassProduction;
	}

	/**
	 * Returns how much GAS a single mass unit of the feed is able to produce at the given day of year, due to is
	 * progressive aging. The 'day_of_year' should be considered as 'days of permanence' in the {@link BioSystem}: the
	 * age in days of the feed. These two
	 * 
	 * @param day_of_year
	 *            The day of the year: ranges from 1 to 365
	 */
	public double getGAS_Loss_on_day_of_year(int day_of_year) {
		return this.experimentalCurveOfDailyGassProduction[day_of_year - 1];
	}

	/**
	 * This method is used in the process of parsing the CLI of BioSystemCLI with the purpose of
	 * creating a new feed from the file name name specified at System prompt.<br/>
	 * For example, specifying 'FEED_MAIS.csv' will cause the system to build a Feed from Class MaisFeed.class.<br/>
	 * The method actually relies on the {@link Feed#getFeedClassFromString} method.
	 * 
	 * @param feedsFileURL
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 */
	public static Feed getFeedFromFilenameURL(URL feedsFileURL) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {

		String filename = feedsFileURL.getFile();

		int startIndex = filename.indexOf(Feed.FEED_FILENAME_PREFIX);
		int endIndex = filename.indexOf(".csv");

		if (startIndex == -1)
			throw new IllegalArgumentException("No 'FEED_' string was found in filename. Could not detect Feed type.");

		if (endIndex == -1)
			throw new IllegalArgumentException("No '.csv' string was found in filename. Could not detect Feed type.");

		if (startIndex > 0 && endIndex > startIndex) {

			String feedString = filename.substring(startIndex + Feed.FEED_FILENAME_PREFIX.length() + 1, endIndex);

			Class<? extends Feed> feedClass = Feed.getFeedClassFromString(feedString);

			return buildFeed(feedClass);

		} else {
			throw new IllegalArgumentException("Unkown error in detecting Feed type from filename. Name must be like 'FEED_MAIS.csv'");
		}

	}

	/**
	 * Picks a file name passed at CLI and strips prefix and suffix
	 * 
	 * @param String
	 *            feedsFileName "FEED_MAIS.csv"
	 * @return
	 */
	public static Feed getFeedFromFilenameString(String feedsFileName) {

		int startIndex = feedsFileName.indexOf(Feed.FEED_FILENAME_PREFIX);
		int endIndex = feedsFileName.indexOf(".csv");

		if (startIndex >= 0 && endIndex > startIndex) {

			String feedString = feedsFileName.substring(startIndex + Feed.FEED_FILENAME_PREFIX.length() + 1, endIndex);

			Class<? extends Feed> feedClass = Feed.getFeedClassFromString(feedString);

			return buildFeed(feedClass);
		}

		return null;

	}

	public static Class<? extends Feed> getFeedClassFromString(String feedString) {

		if (feedString.equals("MAIS")) {
			return MaisFeed.class.asSubclass(Feed.class);

		} else if (feedString.equals("LOIETTO")) {
			return LoiettoFeed.class.asSubclass(Feed.class);

		} else if (feedString.equals("PASTONE-DI-MAIS")) {
			return PastoneDiMaisFeed.class.asSubclass(Feed.class);

		} else if (feedString.equals("POLLINA")) {
			return PollinaFeed.class.asSubclass(Feed.class);

		} else if (feedString.equals("LIQUAME-BOVINO")) {
			return LiquameBovinoFeed.class.asSubclass(Feed.class);

		} else {
			String fullFilePrefix = Feed.FEED_FILENAME_PREFIX + Feed.SPACER;
			throw new InvalidParameterException("\n\n### The specified feed (matrix) file name '" + fullFilePrefix + feedString + "' is not among the allowed ones!!\n\nAllowed file name for CSV matrices files are:\n\n* " + fullFilePrefix + "MAIS\n* " + fullFilePrefix + "LOIETTO\n* " + fullFilePrefix + "POLLINA\n* " + fullFilePrefix + "PASTONE-DI-MAIS\n* " + fullFilePrefix + "LIQUAME-BOVINO\n\nClosing...");
		}
	}

	/**
	 * Build a Feed from a Class<? extends Feed> : used in
	 * 
	 * @param feedClass
	 *            Class<? extends Feed>
	 * @return Feed Returns a Feed or null if an exception is thrown.
	 */
	public static Feed buildFeed(Class<? extends Feed> feedClass) {

		Feed feed = null;
		URL feedLossFileURL = null;
		URL feedGasProductionFileURL = null;
		InputStream feedLossFileInputStream = null;
		InputStream feedGasProductionFileInputStream = null;

		if (feedClass.isAssignableFrom(MaisFeed.class)) {

			if (Utils.isJUnitTest()) {
				// ###################################################################################
				// TODO: This only works when project during JUnit tests!!!
				// ###################################################################################
				// This is needed to instantiate the MaisFeed instance
				feedLossFileURL = BioSystemCLI.class.getResource("./feeds/" + TSS_FILENAME_PREFIX + SPACER + MaisFeed.FEED_NAME + ".csv");
				// This is needed to instantiate the MaisFeed instance aiming to GAS loss computation
				feedGasProductionFileURL = BioSystemCLI.class.getResource("./feeds/" + DAILY_LOSS_TO_GAS_FILENAME_PREFIX + SPACER + MaisFeed.FEED_NAME + ".csv");

			} else {
				// ###################################################################################
				// TODO: This only works when project is packed in JAR but not during JUnit tests!!!
				// ###################################################################################
				// This is needed to instantiate the MaisFeed instance
				feedLossFileInputStream = Feed.class.getResourceAsStream(MaisFeed.defaultLossFileName);
				// This is needed to instantiate the MaisFeed instance aiming to GAS loss computation
				feedGasProductionFileInputStream = Feed.class.getResourceAsStream(MaisFeed.defaultGasProductionFileName);
			}

		} else if (feedClass.isAssignableFrom(LoiettoFeed.class)) {

			if (Utils.isJUnitTest()) {
				// ###################################################################################
				// TODO: This only works when project during JUnit tests!!!
				// ###################################################################################
				// This is needed to instantiate the LoiettoFeed instance
				feedLossFileURL = BioSystemCLI.class.getResource("./feeds/" + TSS_FILENAME_PREFIX + SPACER + LoiettoFeed.FEED_NAME + ".csv");
				// This is needed to instantiate the LoiettoFeed instance aiming to GAS loss computation
				feedGasProductionFileURL = BioSystemCLI.class.getResource("./feeds/" + DAILY_LOSS_TO_GAS_FILENAME_PREFIX + SPACER + LoiettoFeed.FEED_NAME + ".csv");

			} else {
				// ###################################################################################
				// TODO: This only works when project is packed in JAR but not during JUnit tests!!!
				// ###################################################################################
				// This is needed to instantiate the LoiettoFeed instance
				feedLossFileInputStream = Feed.class.getResourceAsStream(LoiettoFeed.defaultLossFileName);
				// This is needed to instantiate the LoiettoFeed instance aiming to GAS loss computation
				feedGasProductionFileInputStream = Feed.class.getResourceAsStream(LoiettoFeed.defaultGasProductionFileName);
			}

		} else if (feedClass.isAssignableFrom(PastoneDiMaisFeed.class)) {

			if (Utils.isJUnitTest()) {
				// ###################################################################################
				// TODO: This only works when project during JUnit tests!!!
				// ###################################################################################
				// This is needed to instantiate the PastoneDiMaisFeed instance
				feedLossFileURL = BioSystemCLI.class.getResource("./feeds/" + TSS_FILENAME_PREFIX + SPACER + PastoneDiMaisFeed.FEED_NAME + ".csv");
				// This is needed to instantiate the PastoneDiMaisFeed instance aiming to GAS loss computation
				feedGasProductionFileURL = BioSystemCLI.class.getResource("./feeds/" + DAILY_LOSS_TO_GAS_FILENAME_PREFIX + SPACER + PastoneDiMaisFeed.FEED_NAME + ".csv");

			} else {
				// ###################################################################################
				// TODO: This only works when project is packed in JAR but not during JUnit tests!!!
				// ###################################################################################
				// This is needed to instantiate the PastoneDiMaisFeed instance
				feedLossFileInputStream = Feed.class.getResourceAsStream(PastoneDiMaisFeed.defaultLossFileName);
				// This is needed to instantiate the PastoneDiMaisFeed instance aiming to GAS loss computation
				feedGasProductionFileInputStream = Feed.class.getResourceAsStream(PastoneDiMaisFeed.defaultGasProductionFileName);
			}

		} else if (feedClass.isAssignableFrom(PollinaFeed.class)) {

			if (Utils.isJUnitTest()) {
				// ###################################################################################
				// TODO: This only works when project during JUnit tests!!!
				// ###################################################################################
				// This is needed to instantiate the PollinaFeed instance
				feedLossFileURL = BioSystemCLI.class.getResource("./feeds/" + TSS_FILENAME_PREFIX + SPACER + PollinaFeed.FEED_NAME + ".csv");
				// This is needed to instantiate the PollinaFeed instance aiming to GAS loss computation
				feedGasProductionFileURL = BioSystemCLI.class.getResource("./feeds/" + DAILY_LOSS_TO_GAS_FILENAME_PREFIX + SPACER + PollinaFeed.FEED_NAME + ".csv");

			} else {
				// ###################################################################################
				// TODO: This only works when project is packed in JAR but not during JUnit tests!!!
				// ###################################################################################
				// This is needed to instantiate the PollinaFeed instance
				feedLossFileInputStream = Feed.class.getResourceAsStream(PollinaFeed.defaultLossFileName);
				// This is needed to instantiate the PollinaFeed instance aiming to GAS loss computation
				feedGasProductionFileInputStream = Feed.class.getResourceAsStream(PollinaFeed.defaultGasProductionFileName);
			}
		} else if (feedClass.isAssignableFrom(LiquameBovinoFeed.class)) {

			if (Utils.isJUnitTest()) {
				// ###################################################################################
				// TODO: This only works when project during JUnit tests!!!
				// ###################################################################################
				// This is needed to instantiate the LiquameBovinoFeed instance
				feedLossFileURL = BioSystemCLI.class.getResource("./feeds/" + TSS_FILENAME_PREFIX + SPACER + LiquameBovinoFeed.FEED_NAME + ".csv");
				// This is needed to instantiate the LiquameBovinoFeed instance aiming to GAS loss computation
				feedGasProductionFileURL = BioSystemCLI.class.getResource("./feeds/" + DAILY_LOSS_TO_GAS_FILENAME_PREFIX + SPACER + LiquameBovinoFeed.FEED_NAME + ".csv");

			} else {
				// ###################################################################################
				// TODO: This only works when project is packed in JAR but not during JUnit tests!!!
				// ###################################################################################
				// This is needed to instantiate the LiquameBovinoFeed instance
				feedLossFileInputStream = Feed.class.getResourceAsStream(LiquameBovinoFeed.defaultLossFileName);
				// This is needed to instantiate the LiquameBovinoFeed instance aiming to GAS loss computation
				feedGasProductionFileInputStream = Feed.class.getResourceAsStream(LiquameBovinoFeed.defaultGasProductionFileName);
			}
		}

		try {
			// feed = feedClass.getConstructor(String.class, URL.class, URL.class).newInstance(name, maizeLossFileURL,
			// maizeGasProductionFileURL);
			if (Utils.isJUnitTest()) {
				Constructor<?> ctor = feedClass.getConstructor(URL.class, URL.class);
				feed = (Feed) ctor.newInstance(new Object[] { feedLossFileURL, feedGasProductionFileURL });
			} else {
				Constructor<?> ctor = feedClass.getConstructor(InputStream.class, InputStream.class);
				feed = (Feed) ctor.newInstance(new Object[] { feedLossFileInputStream, feedGasProductionFileInputStream });

			}

		} catch (InstantiationException e) {
			e.printStackTrace();
			return null;
		} catch (SecurityException e) {
			e.printStackTrace();
			return null;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			return null;
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			return null;
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			return null;
		}

		return feed;

	}

}