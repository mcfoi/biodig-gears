package eu.marcofoi.biodig.biodig_gears.feeds;

import java.io.InputStream;
import java.net.URL;

/**
 * Class representing a {@link Feed} of type 'Pollina' characterized by a specific couple of experimental curves:
 * <ul>
 * <li>The curve of volum_to_mass conversion for deriving the mass of maize available inside a unit volume inside the
 * BioSystem.</li>
 * <li>The curve of daily gas production for mass unit with increasing days age spent in the BioSystem.</li>
 * </ul>
 * 
 * @author Marco Foi
 * @since 12/05/2014
 */
public class PollinaFeed extends Feed {

	public static final String	FEED_NAME						= "POLLINA";

	// This is needed to instantiate the PollinaFeed instance:
	// Paths provided for getResourceAsStream() {relative to internal root of JAR} but
	// automatically added to JAR internal Classpath by Maven so can be in the same pakage, despite in
	// src/main/resources!!!

	// public final static String defaultLossFileName = "eu/marcofoi/biodig/biodig_gears/TSS_POLLINA.csv";
	public final static String	defaultLossFileName			= Feed.TSS_FILENAME_PREFIX + Feed.SPACER + FEED_NAME + ".csv";
	// This is needed to instantiate the MaisFeed instance aiming to GAS loss computation
	public final static String	defaultGasProductionFileName	= Feed.DAILY_LOSS_TO_GAS_FILENAME_PREFIX + Feed.SPACER + FEED_NAME + ".csv";


	/**
	 * Constructor of a Feed (IT: 'matrice'. e.g.: 'mais', 'loietto',etc)</br>
	 * This implementation is the most limited as allows the creation of a feed just providing the mass-degradation
	 * curve. The extended Feed constructor also allows providing the experimental curve of gas production per Matrix
	 * unit mass considering aging.<li>This curve is needed to convert data from a value of Mass in a solution with 90%
	 * of humidity to a dry Mass of matrix since this is the quantity that can be further processed to estimate how much
	 * Gas can be produced (or rather was not produced) from what leaves the BioSystem as a waste.</li>
	 * 
	 * @param MAISFEEDNAME
	 * @param double[] Target array fileTSS_LOST_on_TTQ
	 */
	/*
	 * public MaisFeed(URL fileTSS_LOST_on_TTQ) {
	 * super(MaisFeed.MAISFEEDNAME, fileTSS_LOST_on_TTQ);
	 * }
	 */

	/**
	 * Production constructor that makes use of package embedded resources to initialize the PollinaFeed.
	 * Particularly this constructor uses the two default experimental curves available in the /feeds sub-package:
	 * <ul>
	 * <li>TSS_POLLINA.csv</li>
	 * <li>DAILY_MASS_to_GAS_POLLINA.csv</li>
	 * </ul>
	 */
	public PollinaFeed() {
		super(PollinaFeed.FEED_NAME, defaultLossFileName, defaultGasProductionFileName);
	}

	/**
	 * Testing constructor that provides in the super class a mock GAS production curve filled with "1" values;
	 * Might be used in production where GAS production is of no interest.
	 * 
	 * @param URL
	 *            fileTSS_LOST_on_TTQ
	 */
	@Deprecated
	public PollinaFeed(URL fileTSS_LOST_on_TTQ) {
		super(PollinaFeed.FEED_NAME, fileTSS_LOST_on_TTQ);
	}

	/**
	 * Constructor of a Feed (IT: 'matrice'. e.g.: 'mais', 'loietto',etc)
	 * This implementation allows the creation of a feed providing both the volume-to-mass conversion curve and the
	 * mass-to-produced_gas curve for the specific feed.
	 * <ul>
	 * <li>The first curve is needed to convert data from a value of Mass in a solution with 90% of humidity to a dry
	 * Mass of matrix since this is the quantity that can be further processed to estimate how much Gas can be produced
	 * (or rather was not produced) from what leaves the BioSystem as a waste.</li>
	 * <li>The second curve is required to convert the masses lost by volume subtraction into volumes of gas that was
	 * not produced by the BioSystem. This is particularly important in multi-stage (IT: 'multi-vasca') BioSystems as a
	 * unit mass of feed that moves from one stage (pool) to another keeps aging and this must be taken into account
	 * when considering how much gas that unit might have still produced if it was not subtracted from the system as
	 * part of the ordinary daily process of volume subtraction. In other terms just the first pool of the BioSystem
	 * only receives fresh feeds. All other receive each day a number of feeds of all different ages available in the
	 * preceding pool: each feed will produce gas according to its age. Here the need of converting mass loss (having a
	 * mixture of feeds of different ages, into a daily volume of gas produced)</li>
	 * </ul>
	 * 
	 * @param MAISFEEDNAME
	 * @param fileTSS_LOST_on_TTQ
	 * @param fileExperimentalCureveOfGasProductionPerAgingMassUnit
	 */
	public PollinaFeed(URL fileTSS_LOST_on_TTQ, URL fileExperimentalCureveOfGasProductionPerAgingMassUnit) {
		super(PollinaFeed.FEED_NAME, fileTSS_LOST_on_TTQ, fileExperimentalCureveOfGasProductionPerAgingMassUnit);
	}

	public PollinaFeed(InputStream inputStreamTSS_LOST_on_TTQ, InputStream inputStreamExperimentalCureveOfGasProductionPerAgingMassUnit) {
		super(PollinaFeed.FEED_NAME, inputStreamTSS_LOST_on_TTQ, inputStreamExperimentalCureveOfGasProductionPerAgingMassUnit);
	}

}
