package eu.marcofoi.biodig.biodig_gears;

import java.util.HashMap;

public class FermentationCalculusLogic implements ICalculusLogic {

	private BioSystem				bioSystem;
	private FermentationArrays[]	calcArrays;

	/**
	 * This class implements an ICalclogic behavior to be composed with a Feed class. Its aim is encapsulating the
	 * logic to compute all calculations on the linked InputTons. Specifically it computes the residual Mass, the daily
	 * Output Mass, the tss_lost of the related InputTons and all parameters available in the enum
	 * {@link FermentationArrays}, so that the final set of arrays is somehow the history of the contributes to the
	 * system coming from the input mass in terms of the computed quantities.</br>
	 * The arrays computed from a single InputTons, enumerated in {@link FermentationArrays}, correspond to the
	 * columns of the original XLS file where Marco Negri first implemented the logic. The name of the first file
	 * was:</br>
	 * "Copia di file prova per meteo_ calcolo_fermentatore_simone_28-06.xlsx"</br>
	 * The position i in the arrays corresponds to the (i+1)th-day-of-the-year (e.g.: array[364] is last day of the
	 * year.)
	 * 
	 * @param bs
	 *            The BioSystem to which the FermentationCalculusLogic belongs.
	 *            This coupling is required since the computation needs to access a
	 *            number of parameters from the BioSystem such as its total
	 *            volume or the volumes removed each day by the system managers
	 *            in routine activities.
	 */
	public FermentationCalculusLogic(BioSystem bs) {
		this.bioSystem = bs;
		this.calcArrays = FermentationArrays.values();
	}

	/**
	 * In this method the CalculusLogic is composed with a specific
	 * ICalculatedArrays class through the use of a return value comprising all
	 * the arrays that are declared as being calculated. The Enum is used as the
	 * repository of the knowledge of which arrays are computed by the logic so
	 * that the ICalculusLogic is free from a tight binding with
	 * 
	 * @return An Enum containing the string definitions of all kind of values
	 *         computed on the composed InputTons.
	 */

	public ICalculatedArrays[] getCalculatedArrays() {
		// return FermentationArrays.values();
		return this.calcArrays;
	}

	public Class<? extends Enum> getCalculatedArraysEnum() {
		return FermentationArrays.class;
	}

	/**
	 * Performs the real calculation of all values describing the life of the
	 * InputTons during its first 365 days inside the system.
	 */

	public HashMap<Enum<?>, double[]> calculateFermentationArrays(InputTons inputTons) {
		return calculateFermentationArrays(inputTons, inputTons.getStartTons(), 1);
	}

	/**
	 * Performs the real calculation of all values describing the life of the
	 * InputTons. This method can be used also for inputs that come from
	 * previous system stages hence do not behave like fresh additions to the
	 * system and use the sensible values from the TSSLoss_on_TTQ array so
	 * giving an output of less than 365 computed values.
	 */

	public HashMap<Enum<?>, double[]> calculateFermentationArrays(InputTons inputTons, double startVolume, int startAge) {

		// L
		double[] diluitionArray = bioSystem.getDiluitionSequence();
		// O
		double[] systemDailyTotalOutputArray = bioSystem.getDailyTotalOutputM3();
		// P
		double dailyOutput;
		double[] dailyOutputMassYearArray = new double[BioSystem.computationLengthInDays];
		// Q
		double[] feedLoss = inputTons.getFeedtype().getTSSLoss_on_TTQ_Array();
		int ageShift = startAge - 1;
		// N
		double currentVolume = startVolume;
		double[] residualMassYearArray = new double[BioSystem.computationLengthInDays];
		// R
		double tss_lost;
		double[] tss_lostYearArray = new double[BioSystem.computationLengthInDays];

		for (int i = 0; i < (BioSystem.computationLengthInDays - ageShift); i++) {

			residualMassYearArray[i] = currentVolume;
			// P
			dailyOutput = currentVolume * (systemDailyTotalOutputArray[i] * diluitionArray[i]);
			dailyOutputMassYearArray[i] = dailyOutput;
			// R
			tss_lost = feedLoss[i + ageShift] * currentVolume;
			tss_lostYearArray[i] = tss_lost;

			currentVolume = currentVolume - (dailyOutput + tss_lost);

		}

		HashMap<Enum<?>, double[]> returnMap = new HashMap<Enum<?>, double[]>();

		returnMap.put(FermentationArrays.RESIDUAL_MASS_YEAR_ARRAY, residualMassYearArray);
		returnMap.put(FermentationArrays.DAILY_OUTPUT_MASS_YEAR_ARRAY, dailyOutputMassYearArray);
		returnMap.put(FermentationArrays.TSS_LOST_YEAR_ARRAY, tss_lostYearArray);

		return returnMap;
	}

}