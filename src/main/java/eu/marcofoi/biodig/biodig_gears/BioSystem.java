package eu.marcofoi.biodig.biodig_gears;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Set;

import eu.marcofoi.biodig.biodig_gears.feeds.Feed;
import eu.marcofoi.biodig.biodig_gears.io.CSVOutputWriterMultipleInputTonsWDifferentAgesPerDayBehaviour;
import eu.marcofoi.biodig.biodig_gears.io.CSVOutputWriterSingleFreshInputTonsPerDayBehaviour;
import eu.marcofoi.biodig.biodig_gears.io.IOutputWriterBehavior;
import eu.marcofoi.biodig.biodig_gears.io.InputParser;
import eu.marcofoi.biodig.biodig_gears.io.XLSXOutputWriterSUMValuesOfDayBehaviour;
import eu.marcofoi.biodig.biodig_gears.utils.Utils;

/**
 * This class represent the abstraction of the physical system and has a rather simple initialization: it accepts just
 * two parameters.
 * <ul>
 * <li>The total volume of the system that used to compute dilution factors for the added feeds (which are in m^3)</li>
 * <li>A .csv file containing a column of 365 double values representing volumes subtracted from the system each day of
 * the year.</li>
 * </ul>
 * The typical usage of this Class covers these steps:
 * <ol>
 * <li>Instantiation (using the two above parameters)</li>
 * <li>Addition of feed diaries, in the format of .csv files made by 365-daily-rows, using @link
 * {@link BioSystem#addInputTonsOnePerLoadDayFrom365RowsCSV(URL, ICalculusLogic)}</li>
 * <li>A single call to the @link {@link BioSystem#processInputs()}</li>
 * <li>One ore more calls to one of the 'export' methods to dump the result in a file.</li>
 * </ol>
 * As a n example:</br> <code>
 * URL urlFileOfDailyOutputVolumes = BioSystemCLI.class.getClassLoader().getResource("TOT_OUTPUT_M3.csv");</br> 
 * Biosystem bs = new BioSystem("2550.0", urlFileOfDailyOutputVolumes);</br> 
 * ICalculusLogic calculusLogic = new FermentationAndGasLossCalculusLogic(bs);</br> 
 * URL urlFeedFileMAIZE = BioSystemCLI.class.getClassLoader().getResource("FEED_MAIZE.csv");</br> 
 * URL urlFeedFileLOIETTO = BioSystemCLI.class.getClassLoader().getResource("FEED_LOIETTO.csv");</br> 
 * ...</br> 
 * bs.addInputTonsFrom365RowsCSV(urlFeedFileMAIZE, calculusLogic); // Will use internal experimental curves for gas production</br> 
 * bs.addInputTonsFrom365RowsCSV(urlFeedFileLOIETTO, calculusLogic);</br> 
 * ...</br> 
 * bs.processInputs();</br>
 * // Dump all data from all InputTons of all Feeds as .csv files: one for each computed array</br>
 * bs.exportAllFeedComputation(new CSVOutputWriterSINGLEInputBehaviour());</br> 
 * // Dump all data from all InputTons of the specified Feed as .csv files: one for each computed array</br>
 * bs.exportSingleFeedComputation(MaisFeed.class,new CSVOutputWriterDailySumBehaviour());</br>
 * // Dump all data from all InputTons of the specified Feed as  single .csv file corresponding to the computed array</br>
 * bs.exportSingleFeedComputation(MaisFeed.class, FermentationAndGasArrays.GAS_LOST_YEAR_ARRAY, new CSVOutputWriterDailySumBehaviour());</br>
 * // Dump some results: XLS is limited to 256 columns!</br> 
 * bs.exportAllFeedComputation(new XLSOutputWriterBehaviour());</br> 
 * // Dump some results in XLSX</br> 
 * bs.exportAllFeedComputation(new XLSXOutputWriterSUMandSINGLEInputPerDayBehaviour());</br>
 * </code></br>
 * Note that just .csv guarantees to be able to export all computed data as even XLSX has a limit og 16'000
 * columns.</br>
 * Note that the name of the feeds .csv files MUST adhere to this convention: "FEED_" + <feed name> +
 * ".csv".</br>
 * where the allowed (supported feeds are currently):
 * <ul>
 * <li>MAIZE</li>
 * <li></li>
 * <li></li>
 * </ul>
 * 
 * @author mcfoi
 */
public class BioSystem {

	public static final String		defaultBioSystemName	= "BS";
	public static final int			computationLengthInDays	= 365;

	private String					name					= defaultBioSystemName;
	private double					totalVolumeM3;
	private double[]				totalOutputM3;
	private double[]				diluitionSequence;
	private HashMap<Feed, InputBag>	inputBagsHashMap;

	/**
	 * This class represent the abstraction of the physical system and has a rather simple initialization: it accepts
	 * the total volume of the pool and the history of all withdrawals, as volumes, from the system pool across one year
	 * of usage.</br>
	 * Default BioSystem name used as PREFIX in exported file is "BS".
	 * 
	 * @param totalVolumeM3
	 *            The total volume of the system. Is used to compute dilution
	 *            factors of added feeds (which are in m^3)
	 * @param totalOutputM3CSVFileURL
	 *            A .csv file containing a column of 365 double values
	 *            representing volumes subtracted from the system: one for each
	 *            day of the year.
	 */
	public BioSystem(double totalVolumeM3, URL totalOutputM3CSVFileURL) {

		if (totalVolumeM3 > 0 && totalVolumeM3 <= 1000000.0) {
			this.totalVolumeM3 = totalVolumeM3;
		} else {
			throw new IllegalArgumentException("Total volume cannot be negative or exceed 1 million m3.");
		}

		String filePathString = totalOutputM3CSVFileURL.getPath();

		File totalOutputFile = new File(filePathString);

		try {
			if (totalOutputFile.exists()) {
				this.inputBagsHashMap = new HashMap<Feed, InputBag>();
				this.totalOutputM3 = InputParser.parseSingleColumnCSVFileToDoubleArray(totalOutputFile);
				this.diluitionSequence = computeDiluitionSequance();
			} else {
				throw new IllegalArgumentException("The passed file does not exist!");
			}
		} catch (SecurityException e) {
			throw new IllegalArgumentException("The passed file exists but seems unreadable. Check its permissions!");
		}
	}

	/**
	 * Returns the name of the BioSystem.
	 * Used as prefix in exported files;
	 * 
	 * @return
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the name of the BioSystem.
	 * Used as prefix in exported files;
	 * 
	 * @return
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Returns the double representing the total volume of the system in m^3
	 * 
	 * @return
	 */
	public double getTotalVolumeM3() {
		return this.totalVolumeM3;
	}

	/**
	 * Returns an array representing volumes subtracted from the system: one for
	 * each day of the year
	 * 
	 * @return double[]
	 */
	public double[] getDailyTotalOutputM3() {
		return this.totalOutputM3;
	}

	/**
	 * Allows the addition to the BioSystem of a new list of 365 feeds of the same Feed and starting from day 1 of year
	 * up to day 365, all composed with the same calculation logic.</br>
	 * IMPORTANT NOTE: Be careful that this Feed parser expects a file containing ONLY fresh feeds and ROWS represent
	 * the day_of_year in which the feeds are added to the system. This is DIFFERENT from the file format expected by
	 * {@link #addInputTonsMultiplePerLoadDayFrom365RowsCSV(URL, ICalculusLogic)}
	 * 
	 * @param feedsFileURL
	 *            An URL pointing to a CSV file containing just 365 rows made by double values
	 * @param feed
	 *            The Feed type (e.g.:'maize') to assignto these feeds
	 * @param calculusLogic
	 *            The ICalculus logic to compose with the InputTons to use in calculation
	 */
	public void addInputTonsFrom365RowsCSV(URL feedsFileURL, Feed feed, ICalculusLogic calculusLogic) {

		double[] feedsArray = InputParser.parseSingleColumnCSVFileToDoubleArray(feedsFileURL);

		for (int i = 0; i < feedsArray.length; i++) {
			this.addInputTons(new InputTons(this, feedsArray[i], feed, i + 1), calculusLogic);
		}
	}

	/**
	 * Parses an InputStream pointing to a .CSV File made of one column of double values.
	 * </br>
	 * IMPORTANT NOTE: Be careful that this Feed parser expects a file containing ONLY fresh feeds and ROWS represent
	 * the day_of_year in which the feeds are added to the system. This is DIFFERENT from the file format expected by
	 * {@link #addInputTonsMultiplePerLoadDayFrom365RowsCSV(URL, ICalculusLogic)}
	 * 
	 * @param is
	 * @param calculusLogic
	 * @throws IOException
	 */
	public void addInputTonsFromInputStreamOf365RowsCSV(String feedsFileName, InputStream is, ICalculusLogic calculusLogic) throws IOException {

		double[] feedsArray = InputParser.parseInputStreamToDoubleArray(is);

		Feed feed = Feed.getFeedFromFilenameString(feedsFileName);

		for (int i = 0; i < feedsArray.length; i++) {
			this.addInputTons(new InputTons(this, feedsArray[i], feed, i + 1), calculusLogic);
		}
	}

	/**
	 * This method is a wrapper of the method {@link BioSystem#addInputTonsFrom365RowsCSV(URL, Feed, ICalculusLogic)}
	 * tha extracts the filename from the URL an uses it too detect the correct Feed type to create, without requiring
	 * it to be explicitly declared.</br>
	 * This process relies on the file name structure and requires it to be in teh form of "FEED_feedname.csv", with
	 * 'feedname' being one of those Strings declared in the documentation of {@link BioSystemCLI}. [E.g.:
	 * "FEED_MAIZE.csv"]</br>
	 * IMPORTANT NOTE: Be careful that this Feed parser expects a file containing ONLY fresh feeds and ROWS represent
	 * the day_of_year in which the feeds are added to the system. This is DIFFERENT from the file format expected by
	 * {@link #addInputTonsMultiplePerLoadDayFrom365RowsCSV(URL, ICalculusLogic)}
	 * 
	 * @param feedsFileURL
	 * @param calculusLogic
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	public Feed addInputTonsOnePerLoadDayFrom365RowsCSV(URL feedsFileURL, ICalculusLogic calculusLogic) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {

		double[] feedsArray = InputParser.parseSingleColumnCSVFileToDoubleArray(feedsFileURL);

		Feed feed = Feed.getFeedFromFilenameURL(feedsFileURL);

		System.out.println("\n-> CREATED FEED '"+feed.getFeedName()+"'\n\r");

		for (int i = 0; i < feedsArray.length; i++) {
			this.addInputTons(new InputTons(this, feedsArray[i], feed, i + 1), calculusLogic);
		}

		return feed;
	}

	/**
	 * NOTE: This method WILL NOT WORK for single column feeds that all have same age = 1 (are all fresh!).</br>
	 * This method assumes increasing age with increasing deepness in row level, as for logic coded in
	 * {@link CSVOutputWriterMultipleInputTonsWDifferentAgesPerDayBehaviour} and described at the bottom of this
	 * document.</br>
	 * This method is a wrapper of the method {@link BioSystem#addInputTonsFrom365RowsCSV(URL, Feed, ICalculusLogic)}
	 * that extracts the filename from the URL an uses it to detect the correct Feed type to create, without requiring
	 * it to be explicitly declared.</br>
	 * This process relies on the file name structure and requires it to be in the form of "FEED_feedname.csv", with
	 * 'feedname' being one of those Strings declared in the documentation of {@link BioSystemCLI}. [E.g.:
	 * "FEED_MAIZE.csv"]</br>
	 * Note that this method might not fit for an eventual future implementation of a third stage.</br>
	 * IMPORTANT NOTE: Be careful that this Feed parser expects a data structure DIFFERENT from that of file single
	 * column .csv file in which all feeds are FRESH and the rows represent the day_of_year in which they are added to
	 * the BioSystem.</br>
	 * The data structure expected in the input file is that produced by
	 * {@link CSVOutputWriterSingleFreshInputTonsPerDayBehaviour} that is the following 365x365 matrix:</br>
	 * </br><code>
	 * 0.433905,NaN,NaN,NaN,NaN,..  (365 columns)</br>
	 * 0.326592,0.409799,NaN,NaN,NaN,.. </br>
	 * 0.259825,0.308448,0.369623,NaN,NaN,.. </br>
	 * 0.216119,0.245391,0.278208,0.337482,NaN,.. </br>
	 * ..... </br>
	 * (365 rows: one for each loadDay).. </br>
	 * </code></br>
	 * The AGES of the feeds above, used for computing emitted GAS using experimental curves, are attributed following
	 * this
	 * logic:</br></br><code>
	 * 1day(fresh),NaN,NaN,NaN,NaN,..  (365 columns)</br>
	 * 2days,1day,NaN,NaN,NaN,.. </br>
	 * 3days,2days,1day,NaN,NaN,.. </br>
	 * 4days,3days,2days,1day,NaN,.. </br>
	 * ..... </br>
	 * (365 rows: one for each loadDay).. </br>
	 * </code></br>
	 * 
	 * @param feedsFileURL
	 * @param calculusLogic
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	public Feed addInputTonsMultiplePerLoadDayFrom365RowsCSV(URL feedsFileURL, ICalculusLogic calculusLogic) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {

		double[][] feedsArray = InputParser.parseMultiColumnCSVFileToDoubleArray(feedsFileURL);

		Feed feed = Feed.getFeedFromFilenameURL(feedsFileURL);

		System.out.println("\n-> CREATED FEED '"+feed.getFeedName()+"'\n\r");
		
		// External loop: loop the rows
		for (int row = 0; row < feedsArray.length; row++) {
			// Internal loop: loop the cols
			for (int col = 0; col < feedsArray[row].length; col++) {
				if (!Double.isNaN(feedsArray[row][col])) {
					this.addInputTons(new InputTons(this, feedsArray[row][col], feed, row + 1, row - col + 1), calculusLogic);
				}
			}
		}

		return feed;
	}

	/**
	 * Populates the SECOND STAGE of a BioSystem with feeds created from the output of the FIRST BioSystem stage using
	 * mass outputs coming from {@link #getOutputFeedsMatrix(Feed)}.</br>
	 * Note that this method might not fit for an eventual future implementation of a third stage.</br>
	 * IMPORTANT NOTE: Be careful that this Feed parser expects a data structure DIFFERENT from that of file single
	 * column .csv file in which all feeds are FRESH and the rows represent the day_of_year in which they are added to
	 * the BioSystem.
	 * 
	 * @param data3
	 *            double[0*][day_of_year-row][loadDay][numberOfInputLoadedThatLoadDay] *=there must be just one element
	 *            in the
	 *            top level: the DAILY_OUTPUT_MASS_YEAR_ARRAY
	 */
	public void addInputTonsFromCaclulatedArrayYearMatrix(double[][][][] data, Feed feed, ICalculusLogic calculusLogic) {

		// External loop: cycle in the day_of_year rows
		for (int day_of_year = 0; day_of_year < data[0].length; day_of_year++) {
			// Middle loop: progress through the load days
			for (int loadDay = 0; loadDay < data[0][day_of_year].length; loadDay++) {
				// Internal loop: progress across the different quantities of matrix coming from previous stage (having
				// decreasing age!)
				for (int numberOfInputLoadedThatLoadDay = 0; numberOfInputLoadedThatLoadDay < data[0][day_of_year][loadDay].length; numberOfInputLoadedThatLoadDay++) {
					//
					// Note that the age attributed to the newly created InputTons uses "day_of_year":
					// This WILL NOT WORK ANYMORE
					if (!Double.isNaN(data[0][day_of_year][loadDay][numberOfInputLoadedThatLoadDay])) {
						// --------------------------------------------------------------------------------------------------------//loadDay--------//age
						this.addInputTons(new InputTons(this, data[0][day_of_year][loadDay][numberOfInputLoadedThatLoadDay], feed, day_of_year + 1, day_of_year - loadDay + 1), calculusLogic);
					}
					//
				}
			}
		}
	}

	/**
	 * WARNING: this method is made public mainly for testing purposes! Use addInputTonsFrom*() methods in production
	 * code.</br>
	 * This method is used to add feeds to the system. It requires an InputTons as input and
	 * the calculation logic that will be applied to it.
	 * 
	 * @param input
	 * @param calculusLogic
	 */
	public void addInputTons(InputTons input, ICalculusLogic calculusLogic) {

		Feed ft = input.getFeedtype();

		// Create a new bag if none of type ft already exists
		if (!inputBagsHashMap.containsKey(ft)) {
			addBagIfRequired(new InputBag(BioSystem.this, ft, calculusLogic));
		}

		InputBag bag = inputBagsHashMap.get(ft);

		bag.put(input);
	}

	/**
	 * In the previous check in addInputMTons method does NOT find a bag of type
	 * requested for storing the incoming InputTons, a synchronized check is
	 * made and the proper bag is created
	 * 
	 * @param bag
	 */
	private synchronized void addBagIfRequired(InputBag bag) {
		if (!inputBagsHashMap.containsKey(bag.getBagType())) {
			inputBagsHashMap.put(bag.getBagType(), bag);
		} else {
			throw new UnsupportedOperationException("Trying to add a bag of an already existing type to BioSystem");
		}
	}

	/**
	 * Returns an array representing the dilution sequence of 1 theoretical unit
	 * of volume across a life of 365 days in the system
	 * 
	 * @return double[]
	 */
	public double[] getDiluitionSequence() {
		return diluitionSequence;
	}

	/**
	 * Calculates the dilution sequence for the system starting from its total
	 * volume in m^3.
	 * 
	 * @return double[] An array containing all values of the decreasing
	 *         dilution curve.
	 */
	private double[] computeDiluitionSequance() {

		double currentInputVolume = 1.0;
		double initialDiluition = currentInputVolume / this.totalVolumeM3;
		double residuo = currentInputVolume;
		double diluition = initialDiluition;
		double[] array = new double[computationLengthInDays];

		for (int i = 0; i < computationLengthInDays; i++) {
			array[i] = diluition;
			residuo = residuo - diluition;
			diluition = residuo * initialDiluition;
		}
		return array;
	}

	/**
	 * Triggers computation of calculation logics composed with the ImputsTons
	 * present in the system. This method relies on lower level classes for
	 * execution and constitute a piece of the higher level BioSystem API.
	 */
	public void processInputs() {

		Set<Feed> feedTypesSet = inputBagsHashMap.keySet();

		for (Feed ft : feedTypesSet) {

			// TODO: this can be parallelized
			inputBagsHashMap.get(ft).processInputs();
		}
	}

	/**
	 * This methods causes all data computed for the InputsTons in the system to
	 * be returned in a matrix. The returned matrix has the following tree:</br>
	 * double[feedType][calculatedArray][day_of_year-row][loadDay][#
	 * OfInputLoadedThatLoadDay]</br>
	 * The [calculatedArray] is a varying-size array that is made of all quantities that have been calculated starting
	 * from the InputTons and that are listed by the {@link ICalculatedArrays} associated to the {@link ICalculusLogic}
	 * used for computing results. Each Feed can have its own logic but it is more reasonable to use the same logic for
	 * performing calculations for all Feeds added to the BioSystem.
	 * 
	 * @return double[feedType][calculatedArray][day_of_year-row][loadDay][#
	 *         OfInputLoadedThatLoadDay]
	 */
	public double[][][][][] getAllFeedsComputation() {

		Set<Feed> feedTypesSet = inputBagsHashMap.keySet();
		double[][][][][] fullOutputOfAllFeeds = new double[feedTypesSet.size()][][][][];
		int i = 0;
		for (Feed ft : feedTypesSet) {
			fullOutputOfAllFeeds[i] = getSingleFeedComputation(ft);
			i++;
		}
		return fullOutputOfAllFeeds;
	}

	/**
	 * A private method that carries out the data extraction for a single feed
	 * type.</br>
	 * The [calculatedArray] is a varying-size array that is made of all quantities that have been calculated starting
	 * from the InputTons and that are listed by the {@link ICalculatedArrays} associated to the {@link ICalculusLogic}
	 * used for computing results. Each Feed can have its own logic but it is more reasonable to use the same logic for
	 * performing calculations for all Feeds added to the BioSystem.
	 * 
	 * @param feedType
	 *            The feed type to export
	 * @return double[one of the calculated arrays][day_of_year-row][loadDay][#OfInputLoadedThatLoadDay]
	 *         A matrix containing a stack of the n bi-dimensional matrices computed in the calculation, one for each
	 *         value of the {@link ICalculatedArrays} Enum.
	 */
	public double[][][][] getSingleFeedComputation(Feed feedType) {

		InputBag bag = inputBagsHashMap.get(feedType);

		double[][][][] fullOutputOfFeed = bag.getComputation();

		return fullOutputOfFeed;
	}

	/**
	 * This methods returns the information contained in the requested array for one single Feed type
	 * 
	 * @param feedType
	 * @param dailyYearArray
	 *            One of the ICalculatedArrays used by the ICalculusLogic combined with the Feed for performing
	 *            calulations.
	 * @return double[0*][day_of_year-row][loadDay][numberOfInputLoadedThatLoadDay] *=there is just one element in the
	 *         top level: the requested dailyYearArray
	 */
	public double[][][][] getSingleFeedSingleArrayComputation(Feed feedType, FermentationAndGasArrays dailyYearArray) {

		InputBag bag = inputBagsHashMap.get(feedType);

		double[][][][] fullOutputOfFeed = bag.getComputation(dailyYearArray);

		return fullOutputOfFeed;
	}

	/**
	 * This method is a utility method wrapping
	 * {@link BioSystem#getSingleFeedSingleArrayComputation(Feed, FermentationAndGasArrays)} for retrieving just the
	 * mass values (DAILY_OUTPUT_MASS_YEAR_ARRAY) that, being subtracted from the BioSystem as a consequnce of the
	 * ordinary volume output, can be conveyed in a second BioSystem and there create new InputTons.
	 * 
	 * @param feedType
	 *            Feed
	 * @return double[0*][day_of_year-row][loadDay][numberOfInputLoadedThatLoadDay] *=there is just one element in the
	 *         top level: the DAILY_OUTPUT_MASS_YEAR_ARRAY
	 */
	public double[][][][] getOutputFeedsMatrix(Feed feedType) {

		InputBag bag = inputBagsHashMap.get(feedType);

		double[][][][] fullOutputOfFeed = bag.getComputation(FermentationAndGasArrays.DAILY_OUTPUT_MASS_YEAR_ARRAY);

		return fullOutputOfFeed;
	}

	/**
	 * This methods causes all data computed for the InputsTons in the system to
	 * be dumped into files according with the passed write behavior. The
	 * returned matrix has the following tree:
	 * <ul>
	 * <li>root</li>
	 * <ul>
	 * <li>feedType0(),</li>
	 * <li>...,</li>
	 * <li>feedtypeN(</li>
	 * <ul>
	 * <li>computedArray0(),</li>
	 * <li>...,</li>
	 * <li>computedArrayN(matrixOfValues)</li> )
	 * </ul>
	 * </ul> </ul> or laid in another way: <br/>
	 * double[feedType][calculatedArray][day_of_year-row][loadDay][#OfInputLoadedThatLoadDay]
	 * 
	 * @param writerBehavior
	 * @return
	 *         double[feedType][calculatedArray][day_of_year-row][loadDay][#OfInputLoadedThatLoadDay]
	 */
	public double[][][][][] exportAllFeedComputation(IOutputWriterBehavior writerBehavior) {

		Set<Feed> feedTypesSet = inputBagsHashMap.keySet();
		double[][][][][] fullOutputOfAllFeeds = new double[feedTypesSet.size()][][][][];
		int i = 0;
		for (Feed feedType : feedTypesSet) {
			fullOutputOfAllFeeds[i] = exportFeedComputation(feedType, writerBehavior);
			i++;
		}
		return fullOutputOfAllFeeds;
	}

	/**
	 * This methods causes all data computed for the InputsTons in the system belonging to the supplied {@link Feed} to
	 * be dumped into files according with the passed write behavior. The returned matrix has the following tree:</br>
	 * <ul>
	 * <li>root</li>
	 * <ul>
	 * <li>selectedFeedType(</li>
	 * <ul>
	 * <li>computedArray0(),</li>
	 * <li>...,</li>
	 * <li>computedArrayN(matrixOfValues)</li> )
	 * </ul>
	 * </ul> </ul> or laid in another way: <br/>
	 * double[selectedFeedType][calculatedArray][day_of_year-row][loadDay][#OfInputLoadedThatLoadDay]
	 * 
	 * @param Class
	 *            <? extends Feed> feedClass: the Class object that will be used to select which matrices will be
	 *            exported among all those computed.
	 * @param IOutputWriterBehavior
	 *            The {@link IOutputWriterBehavior} (e.g.: {@link CSVOutputWriterSingleFreshInputTonsPerDayBehaviour},
	 *            {@link XLSXOutputWriterSUMValuesOfDayBehaviour}) that will control how computed data will be exported
	 *            to filesystem.
	 * @return
	 *         double[feedType][calculatedArray][day_of_year-row][loadDay][#OfInputLoadedThatLoadDay]
	 */
	public double[][][][][] exportSingleFeedComputation(Class<? extends Feed> feedClass, IOutputWriterBehavior writerBehavior) {

		Set<Feed> feedTypesSet = inputBagsHashMap.keySet();
		double[][][][][] fullOutputOfAllFeeds = new double[feedTypesSet.size()][][][][];
		int i = 0;
		for (Feed feedType : feedTypesSet) {

			if (feedClass.isAssignableFrom(feedType.getClass())) {

				fullOutputOfAllFeeds[i] = exportFeedComputation(feedType, writerBehavior);
				i++;
			}
		}
		return fullOutputOfAllFeeds;

	}

	/**
	 * This methods causes all data computed for the InputsTons in the system belonging to the supplied {@link Feed} to
	 * be dumped into files according with the passed write behavior. The returned matrix has the following tree:</br>
	 * <ul>
	 * <li>root</li>
	 * <ul>
	 * <li>selectedFeedType(</li>
	 * <ul>
	 * <li>computedArraySelectedForExport(matrixOfValues)</li> )
	 * </ul>
	 * </ul> </ul> or laid in another way: <br/>
	 * double[selectedFeedType][toBeDumpedArray][day_of_year-row][loadDay][#OfInputLoadedThatLoadDay]
	 * 
	 * @param Class
	 *            <? extends Feed> feedClass: the Class object that will be used to select which matrices will be
	 *            exported among all those computed.
	 * @param ICalculatedArrays
	 *            toBeDumpedArray
	 *            The array that will be dumped to file, among all those computed for the selected Feed, as specified in
	 *            the {@link ICalculusLogic} coupled with the involved {@link InputBag}
	 * @param IOutputWriterBehavior
	 *            The {@link IOutputWriterBehavior} (e.g.: {@link CSVOutputWriterSingleFreshInputTonsPerDayBehaviour},
	 *            {@link XLSXOutputWriterSUMValuesOfDayBehaviour}) that will control how computed data will be exported
	 *            to filesystem.
	 * @return
	 *         double[feedType][calculatedArray][day_of_year-row][loadDay][#OfInputLoadedThatLoadDay]
	 */
	public double[][][][][] exportSingleFeedComputation(Class<? extends Feed> feedClass, ICalculatedArrays toBeDumpedArray, IOutputWriterBehavior writerBehavior) {

		Set<Feed> feedTypesSet = inputBagsHashMap.keySet();
		double[][][][][] fullOutputOfAllFeeds = new double[feedTypesSet.size()][][][][];
		int i = 0;
		for (Feed feedType : feedTypesSet) {

			if (feedClass.isAssignableFrom(feedType.getClass())) {

				fullOutputOfAllFeeds[i] = exportFeedComputation(feedType, toBeDumpedArray, writerBehavior);
				i++;
			}
		}
		return fullOutputOfAllFeeds;

	}

	/**
	 * A private method that carries out the export process for a single feed
	 * type
	 * 
	 * @param feedType
	 *            The feed type to export
	 * @param writerBehavior
	 *            The write behavior that will determine what actual output will
	 *            be produced.
	 * @return double[][][][] A matrix containing a stack of the n
	 *         bi-dimensional matrices computed in the calculation: one for each
	 *         value of the {@link ICalculatedArrays} Enum.
	 */
	private double[][][][] exportFeedComputation(Feed feedType, IOutputWriterBehavior writerBehavior) {

		InputBag bag = inputBagsHashMap.get(feedType);

		double[][][][] fullOutputOfFeed = bag.exportComputation(writerBehavior);

		return fullOutputOfFeed;
	}

	/**
	 * A private method that carries out the export process for a single feed
	 * type
	 * 
	 * @param feedType
	 *            The feed type to export
	 * @param writerBehavior
	 *            The write behavior that will determine what actual output will
	 *            be produced.
	 * @param Enum
	 *            of ICalculatedArrays: allows selection of which array to export.
	 * @return double[][][][] A matrix containing a stack of the n
	 *         bi-dimensional matrices computed in the calculation: one for each
	 *         value of the {@link ICalculatedArrays} Enum.
	 */
	private double[][][][] exportFeedComputation(Feed feedType, ICalculatedArrays toBeExportedArray, IOutputWriterBehavior writerBehavior) {

		InputBag bag = inputBagsHashMap.get(feedType);

		double[][][][] fullOutputOfFeed = bag.exportComputation(toBeExportedArray, writerBehavior);

		return fullOutputOfFeed;
	}

	/**
	 * Returns the number of InputsTons loaded in the whole system
	 * 
	 * @return
	 */
	public int countInputs() {

		Set<Feed> feedTypesSet = inputBagsHashMap.keySet();
		int inputCount = 0;
		for (Feed ft : feedTypesSet) {
			inputCount += inputBagsHashMap.get(ft).countInputTons();
		}
		return inputCount;
	}

}
