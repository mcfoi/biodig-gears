package eu.marcofoi.biodig.biodig_gears.utils;

import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

public class Utils {

		/**
	 * Round decimal according to String format
	 * 
	 * @param d
	 *            double
	 * @param fmt
	 *            String format as "#.##"
	 * @return
	 */
	public static String roundDecimal(double d, String fmt) {

		DecimalFormat df = new DecimalFormat(fmt);
		df.setRoundingMode(RoundingMode.DOWN);
		String s = df.format(d);
		return s;
	}

	public static DecimalFormat getDecimalFormat(String formatString, DecimalFormatSymbols decimalFormatSymbols) {

		// DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols(Locale.US);
		// otherSymbols.setDecimalSeparator('.');
		// otherSymbols.setGroupingSeparator('\'');
		DecimalFormat df = new DecimalFormat(formatString, decimalFormatSymbols);
		df.setRoundingMode(RoundingMode.DOWN);
		return df;
	}

	public static String getDateAsString() {

		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
		Date date = new Date();
		return dateFormat.format(date);

	}

	public static boolean isJUnitTest() {

		boolean junit = false;
		StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
		List<StackTraceElement> list = Arrays.asList(stackTrace);

		ListIterator<StackTraceElement> iterator = list.listIterator();

		while (iterator.hasNext()) {
			String line = iterator.next().getClassName();
			// String line = list.get(list.size() - 1).getClassName();
			if (line.contains("junit")) {
				junit = true;
			}
		}

		if (junit) {
			System.out.println("\n\rINSIDE JUNIT!");
		} else {
			System.out.println("\n\rNOT INSIDE JUNIT!");
		}

		return junit;
	}

}
