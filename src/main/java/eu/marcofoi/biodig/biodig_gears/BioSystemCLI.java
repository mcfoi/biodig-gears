package eu.marcofoi.biodig.biodig_gears;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import eu.marcofoi.biodig.biodig_gears.feeds.Feed;
import eu.marcofoi.biodig.biodig_gears.io.CSVOutputWriterDailySumBehaviour;
import eu.marcofoi.biodig.biodig_gears.io.CSVOutputWriterMultipleInputTonsWDifferentAgesPerDayBehaviour;
import eu.marcofoi.biodig.biodig_gears.io.CSVOutputWriterSameLifespanSumBehaviour;
import eu.marcofoi.biodig.biodig_gears.io.CSVOutputWriterSingleFreshInputTonsPerDayBehaviour;
import eu.marcofoi.biodig.biodig_gears.io.InputParser;
import eu.marcofoi.biodig.biodig_gears.io.XLSXOutputWriterSUMValuesOfDayBehaviour;

public class BioSystemCLI {

	public static final String	VER				= "2.2";
	public static final String	OUTPUT_FOLDER	= "Output/";

	/**
	 * A command line interface for using BioSystem.</br>
	 * The parameters passed to command line are:
	 * <ul>
	 * <li>BioSystem volume: will be parsed to a double</li> *
	 * <li>Name of .csv file containing a list of volumes removed from the BioSystem: one for each day of year</li>
	 * <li>Name of .csv file containing a list of masses of a specific matrix (e.g.: mais) added to the BioSystem: one
	 * value for each day of year</li>
	 * <li>Name of .csv file other file as preceding but or different matrix (e.g.: loietto)</li>
	 * </ul>
	 * NOTE: file name of feeds (matrices) must have following schema "FEED_MATRIXNAME.csv": the MATRIXNAME will be used
	 * to select correct experimental curves available in the software for performing calculations.</br>
	 * Currently the BioSystem supports the following matrices:</br>
	 * TODO UPDATE THI LISTING
	 * <ul>
	 * <li>MAIZE</li>
	 * <li>...</li>
	 * <li>...</li>
	 * <li>...</li>
	 * </ul>
	 * Last boolean determines whether the list of daily mass outputs of each matrix is fed into a second BioSystem (a
	 * second stage of the first) for further processing.
	 * e.g.: java -jar BioSystem.jar 2550.0 fileOfSystemDailyVolumeOutput.csv FEED_matrix_1_name.csv ...
	 * FEED_matrix_N_name.csv true|false
	 * 
	 * @param args
	 * @throws java.io.IOException
	 * @throws java.io.FileNotFoundException
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	public static void main(String... args) throws java.io.IOException, java.io.FileNotFoundException, IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {

		String cancelletti = "#######################################################################";
		BioSystem bs = null;
		boolean doComputeSecondStage = false;
		double totalVolumeM3;
		URL urlFileOutputDailyVolumes = null;
		List<Feed> feedList = new ArrayList<Feed>();

		Long start = System.currentTimeMillis();

		System.out.println("");
		System.out.println("#######################################################################");
		System.out.println("#                         BioSystem v."+VER+"                             #");
		System.out.println("#######################################################################");
		System.out.println("");

		if (args == null) {
			throw new InvalidParameterException("\nNo parameter was specified in command line. E.g.: java -jar BioSystem.jar 2550.0 TOT_OUTPUT_M3.csv FEED_MAIS.csv ... FEED_LOIETTO.csv true\nAllowed matices are:\n\n* MAIS\n* LOIETTO\n* POLLINA\n* PASTONE-DI-MAIS\n* LIQUAME-BOVINO\n\nClosing...");
		} else if (args.length < 4) {
			throw new InvalidParameterException("\nNot enough parameters were specified!\n\nMinimum set of parameters are:\n\n * volume of BioSystem\n * CSV file of daily volume subtractions\n * at least one CSV file of a feed (matix)\n * true|false to control second stage processing\n\nE.g.: java -jar BioSystem.jar 2550.0 TOT_OUTPUT_M3.csv FEED_MAIS.csv false.\n\n### Check your command in START.bat file!!.");
		} else {

			ICalculusLogic calculusLogic = null;
			int numColsInInputFile = 0;
			int argnum = 0;

			// ###################### START FIRST STAGE ###################

			System.out.println("");
			System.out.println("#######################################################################");
			System.out.println("#                           FIRST STAGE                               #");
			System.out.println("#######################################################################");
			System.out.println("");

			// SEEK in the command-line arguments and seek for BioSystem init parameters

			// LOOK in FIRST argument: BioSysltem VOLUME (m^3)
			try {
				totalVolumeM3 = Double.parseDouble(args[0]);
			} catch (Exception e) {
				throw new NumberFormatException("\n\n" + cancelletti + "\n### The first argument of command (" + args[0] + ") could not be identified (parsed) as a double.\n### It must define the volume of the BioSystem so it should have been\n### something like '2550.0' (without quotes).\n### Check your command in START.bat file!!.\n" + cancelletti);
			}
			System.out.println("\n-> System Volume: " + Double.toString(totalVolumeM3));

			// LOOP in remaining command-line arguments
			for (String volOutputOrFeedDiaryFile : args) {

				// BioSysltem VOLUME (m^3)
				if (argnum == 1) {

					String path = new File(".").getAbsolutePath();
					System.out.println("\n-> VOLUME SUBTRACTIONS: using file " + path + "\\" + volOutputOrFeedDiaryFile);

					// While debugging in ECLIPSE, the host folder HAS to be added in
					// "Debug Configurations -> Classpath"
					// URL urlFileOutputDailyVolumes = BioSystemCLI.class.getResource(volOutputOrFeedDiaryFile);

					File file = new File(path + "\\" + volOutputOrFeedDiaryFile);

					urlFileOutputDailyVolumes = file.toURI().toURL();

					if (file.canRead()) {
						bs = new BioSystem(totalVolumeM3, urlFileOutputDailyVolumes);
					}
				}

				// Feeds : build them from other CLI parameters [e.g.: "FEED_MAIZE.csv"].
				if (argnum > 1 && argnum < args.length - 1) {

					String path = new File(".").getAbsolutePath();
					System.out.println("\n-> FEED " + (argnum - 1) + ": using file " + path + "\\" + volOutputOrFeedDiaryFile);

					calculusLogic = new FermentationAndGasLossCalculusLogic(bs);
					// While debugging in ECLIPSE, the host folder HAS to be added in
					// "Debug Configurations -> Classpath"
					// URL urlFeedFile = BioSystemCLI.class.getResource(volOutputOrFeedDiaryFile);

					File file = new File(path + "\\" + volOutputOrFeedDiaryFile);
					// URL urlFileOutputDailyVolumes2 = file.toURI().toURL();

					URL urlFeedFile = file.toURI().toURL();

					// InputStream is =
					// BioSystemCLI.class.getResourceAsStream("eu/marcofoi/biodig/biodig_gears/"+volOutputOrFeedDiaryFile);

					// System.out.println("INPUT STREAM: "+Integer.toString(is.available()));

					if (file.canRead()) {

						numColsInInputFile = InputParser.getCols(urlFeedFile);

						Feed feed = null;

						if (numColsInInputFile == 1) {
							// Add inputTons from input file
							System.out.println("\n-> Reading from a SINGLE COLUM feed file...");
							feed = bs.addInputTonsOnePerLoadDayFrom365RowsCSV(urlFeedFile, calculusLogic);
						} else if (numColsInInputFile > 1) {
							// Add inputTons from multiple columns file
							System.out.println("\n-> Reading from a MULTI COLUM feed file...");
							feed = bs.addInputTonsMultiplePerLoadDayFrom365RowsCSV(urlFeedFile, calculusLogic);
						} else {
							throw new IllegalArgumentException("\n\n" + cancelletti + "\n### Input File " + volOutputOrFeedDiaryFile + " has an uncomprehensible content.\n### Please check it!\n" + cancelletti);
						}
						// Add feeds to list for eventul process thel later in second stage.
						feedList.add(feed);
					} else {
						throw new InvalidParameterException("\n\n" + cancelletti + "\n### Could not read file " + path + "\\" + volOutputOrFeedDiaryFile + "\n### Check file exists and is readable (is not open in other software).\n" + cancelletti);
					}

				}

				// Process LAST parameter seeking for a boolean to decide whether compute second stage.
				if (argnum == args.length - 1) {

					doComputeSecondStage = Boolean.parseBoolean(volOutputOrFeedDiaryFile);
				}

				argnum++;
			}

			// ####
			// Process BioSystem content:
			bs.processInputs();
			//
			// ####

			// ####
			// Export results to file

			// Export full result in .CSV (big files!!)
			if (numColsInInputFile == 1) {
				// Export inputTons coherently with the single-column input file
				bs.exportAllFeedComputation(new CSVOutputWriterSingleFreshInputTonsPerDayBehaviour());
			} else if (numColsInInputFile > 1) {
				// Export inputTons coherently with the multiple-column input file
				bs.exportAllFeedComputation(new CSVOutputWriterMultipleInputTonsWDifferentAgesPerDayBehaviour());
				// Export Sums of same-lifespan contributes in .CSV
				bs.exportAllFeedComputation(new CSVOutputWriterSameLifespanSumBehaviour());
			}

			// Export handy Sums in .CSV
			// REMOVED: export is too slow and Sums are altredy exported in Excel. See below.
			// bs.exportAllFeedComputation(new CSVOutputWriterDailySumBehaviour());

			// Export handy Sums in Excel
			bs.exportAllFeedComputation(new XLSXOutputWriterSUMValuesOfDayBehaviour());

			// bs.exportSingleFeedComputation(MaisFeed.class,new CSVOutputWriterDailySumBehaviour());

			// ###################### END FIRST STAGE ###################

			// ###################### START SECOND STAGE ################

			if (doComputeSecondStage) {

				System.out.println("");
				System.out.println("#######################################################################");
				System.out.println("#                           SECOND STAGE                              #");
				System.out.println("#######################################################################");
				System.out.println("");

				// Cycle in all processed feeds
				for (Feed feed : feedList) {

					double[][][][] data = bs.getOutputFeedsMatrix(feed);

					// Create SECOND STAGE
					BioSystem bs2ndstage = new BioSystem(totalVolumeM3, urlFileOutputDailyVolumes);

					// Set a not-default name (would be "BS") to help tracking output in filnames.
					bs2ndstage.setName("BS2s");

					// Add inputTons from output of previous stage
					bs2ndstage.addInputTonsFromCaclulatedArrayYearMatrix(data, feed, calculusLogic);

					// Destroy FIRST STAGE to save memory
					// bs = null;

					// Launch input processing
					bs2ndstage.processInputs();

					// Export handy Sums in .CSV
					// REMOVED: export is too slow and Sums are altredy exported in Excel. See below.
					// bs2ndstage.exportAllFeedComputation(new CSVOutputWriterDailySumBehaviour());

					// Export handy Sums in Excel
					bs2ndstage.exportAllFeedComputation(new XLSXOutputWriterSUMValuesOfDayBehaviour());

					// Export full result in .CSV (big files!!)
					bs2ndstage.exportAllFeedComputation(new CSVOutputWriterMultipleInputTonsWDifferentAgesPerDayBehaviour());
					// Export Sums of same-lifespan contributes in .CSV
					bs.exportAllFeedComputation(new CSVOutputWriterSameLifespanSumBehaviour());

				}

			}
			// ###################### ENS SECOND STAGE ################
		}

		System.out.println("");
		System.out.println("#######################################################################");
		System.out.println("         Execution completed! Total time elapsed: " + ((System.currentTimeMillis() - start) / 1000L) + "s");
		System.out.println("                          BioSystem v."+VER+"                              ");
		System.out.println("#######################################################################");
		System.out.println("");
	}
}
