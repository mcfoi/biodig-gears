package eu.marcofoi.biodig.biodig_gears.io;

public interface IOutputWriterBehavior {
	
	public void saveYearMatrixToFile(double[][][] yearMatrix, String filePath);

	public String getOutputFileExtension();

	public String getSuffix();

}
