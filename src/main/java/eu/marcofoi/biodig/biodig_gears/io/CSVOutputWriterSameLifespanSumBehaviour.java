package eu.marcofoi.biodig.biodig_gears.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import eu.marcofoi.biodig.biodig_gears.InputTons;

/**
 * Class suitable for exporting data from a BioSystem fed with MULTIPLE (but
 * works also for SINGLE) {@link InputTons} per Feed per load day. The whole
 * yearly matrix for a single feed type is crossed and a sum of all contributes
 * that spend the same amount of time in the system is returned. The final
 * result is an array of 365 values whose first value is the sum of all
 * contributes that spend 1 day in the system before being extracted.. ..while
 * the 365th one is a sum of all those spending 365 days (so, actually, just
 * one!)
 * 
 * @author Marco Foi
 */
public class CSVOutputWriterSameLifespanSumBehaviour extends
		CSVOutputWriterAbstractyBehaviour implements IOutputWriterBehavior {

	/**
	 * This suffix is used in exporting files inside InputBag export methods.
	 * 
	 * @return
	 */

	public String getSuffix() {
		return "SAMELIFESPANSUM-DAY1TO365th";
	}

	/**
	 * The data matrix is composed by:</br>
	 * <ul>
	 * <li>Row: the day of the year</li>
	 * <li>Column: the different InputTons available in the system in that
	 * day_of_year</li>
	 * <li>The value of the computed array for that InputTons: e.g.: the
	 * resudual mass remaining of an InputTons on that day_of_year</li>
	 * </ul>
	 */

	public void saveYearMatrixToFile(double[][][] yearMatrix, String filePath) {
		
		double[] sameLifespanSums = new double[365];

		try {
			File file = new File(filePath);

			// if file doesn't exists, then create it
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);

			int numRows = yearMatrix.length;
			for (int row = 0; row < numRows; row++) {
				//String stringRow = "";
				//double dailySum = 0.0;
				int numCols = yearMatrix[row].length;
				Double value;
				//for (int col = 0; col < numCols; col++) {
				// Pic last column
				double[] colarray = yearMatrix[row][row];
					// Cycle in column
					for (int i = (colarray.length-1); i >= 0; i--) {
						value = colarray[i];
						if(!value.equals(Double.NaN)) {
							int y = row-i;						
							sameLifespanSums[y] = sameLifespanSums[y] + value;
						}
					}
				//}
			}
			
			String spacer = "";
			String stringRow = "";
			// Cycle in the array to get its String representation
			for(int y = 0; y < sameLifespanSums.length; y++){
				
				stringRow += spacer.concat(/*df.format(*/Double.toString(sameLifespanSums[y]));
				spacer = ",";
				
			}
			
			bw.write(stringRow);			
			bw.close();

			System.out.println("Done! End writing file: " + filePath);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
