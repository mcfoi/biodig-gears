package eu.marcofoi.biodig.biodig_gears.io;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import eu.marcofoi.biodig.biodig_gears.utils.Utils;

@Deprecated
/**
 * This method export to an OLD Excel format the data matrix. Given the fact
 * that .xls file cannot exceed 255 columns this write behavior is
 * deprecated as it would truncate the output to column IV so loosing all
 * columns from 256 to 365. More it does not handle the case of more than
 * one input for a single loadDay but simply returns only the first input
 * loaded.
 * @author mcfoi
 *
 */
public class XLSOutputWriterBehaviour implements IOutputWriterBehavior {

	private WritableCellFormat times = new WritableCellFormat();


	public String getOutputFileExtension() {
		return ".xls";
	}
	
	/**
	 * This suffix is used in exporting files inside InputBag export methods.
	 * @return
	 */

	public String getSuffix(){
		return "DAILYSUM";
	}

	/**
	 * This method export to an OLD Excel format the data matrix. Given the fact
	 * that .xls file cannot exceed 255 columns this write behavior is
	 * deprecated as it would truncate the output to column IV so loosing all
	 * columns from 256 to 365. More it does not handle the case of more than
	 * one input for a single loadDay but simply returns only the first input
	 * loaded.
	 */

	public void saveYearMatrixToFile(double[][][] yearMatrix, String filePath) {

		try {
			File file = new File(filePath);
			file.getParentFile().mkdirs();
			
			WorkbookSettings wbSettings = new WorkbookSettings();
			
			//Suppres warnings if run from JUnits Tests: would slow down testing a lot!
			wbSettings.setSuppressWarnings(Utils.isJUnitTest());
			
			wbSettings.setLocale(new Locale("en", "EN"));

			WritableWorkbook workbook = Workbook.createWorkbook(file,
					wbSettings);
			workbook.createSheet("Report", 0);
			WritableSheet excelSheet = workbook.getSheet(0);
			// createLabel(excelSheet);
			createContent(excelSheet, yearMatrix);

			workbook.write();
			workbook.close();

		} catch (WriteException e) {
			e.printStackTrace();
		}  catch (IOException e){
			e.printStackTrace();
		}
		
		System.out.println("Done! End writing file: "+filePath);
	}

	private void createContent(WritableSheet excelSheet, double[][][] yearMatrix)
			throws RowsExceededException, WriteException {

		long lStartTime = System.nanoTime();
		for (int row = 0; row < yearMatrix.length; row++) {
			for (int col = 0; col < yearMatrix[row].length; col++) {
				// Here the first value (corresponding to the first input for the
				// loadDay) is returned
				addNumber(excelSheet, col, row, yearMatrix[row][col][0]);
			}
		}
		long lEndTime = System.nanoTime();
		long difference = lEndTime - lStartTime;
		System.out.println("Elapsed milliseconds: " + difference / 1000000);
		/*
		 * // Write a few number for (int i = 1; i < 10; i++) { // First column
		 * addNumber(excelSheet, 0, i, i + 10); // Second column
		 * addNumber(excelSheet, 1, i, i * i); }
		 */
	}

	private void addNumber(WritableSheet sheet, int column, int row,
			double value) throws WriteException, RowsExceededException {
		Number number;
		number = new Number(column, row, value, times);
		sheet.addCell(number);
	}

}
