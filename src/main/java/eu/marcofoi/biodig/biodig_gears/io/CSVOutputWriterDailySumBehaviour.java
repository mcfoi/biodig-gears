package eu.marcofoi.biodig.biodig_gears.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import eu.marcofoi.biodig.biodig_gears.InputTons;

/**
 * Class suitable for exporting data from a BioSystem fed with MULTIPLE (but works also for SINGLE) {@link InputTons}
 * per Feed per load day. All data for a single day of year is returned summed.
 * 
 * @author Marco Foi
 */
public class CSVOutputWriterDailySumBehaviour extends CSVOutputWriterAbstractyBehaviour implements IOutputWriterBehavior {

	/**
	 * This suffix is used in exporting files inside InputBag export methods.
	 * 
	 * @return
	 */

	public String getSuffix() {
		return "DAILYSUM";
	}

	/**
	 * The data matrix is composed by:</br>
	 * <ul>
	 * <li>Row: the day of the year</li>
	 * <li>Column: the different InputTons available in the system in that day_of_year</li>
	 * <li>The value of the computed array for that InputTons: e.g.: the resudual mass remaining of an InputTons on that
	 * day_of_year</li>
	 * </ul>
	 */

	public void saveYearMatrixToFile(double[][][] yearMatrix, String filePath) {

		try {
			File file = new File(filePath);

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);

			for (int row = 0; row < yearMatrix.length; row++) {
				String stringRow = "";
				double dailySum = 0.0;
				for (int col = 0; col < yearMatrix[row].length; col++) {
					// #############################################################
					// Here ALL the values from all the InputTons available in the accessed loadDAy
					// are summed to be returned where a returning each single value would be useless
					// E.g.: The total daily GAS not emitted is more important than the list of all contributes
					// to that quantity from each InputTons
					// #############################################################
					// Loop in the different values from InputTons loaded on the same load day
					for (int inputTon = 0; inputTon < yearMatrix[row][col].length; inputTon++) {

						if (!Double.isNaN(yearMatrix[row][col][inputTon])) {
							dailySum += yearMatrix[row][col][inputTon];
						}

					}
				}
				stringRow = df.format(dailySum);
				bw.write(stringRow);
				bw.newLine();
			}
			bw.close();

			System.out.println("Done! End writing file: "+filePath);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
