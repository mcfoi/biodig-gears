package eu.marcofoi.biodig.biodig_gears.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import eu.marcofoi.biodig.biodig_gears.InputTons;

/**
 * Class suitable for exporting data from a BioSystem fed with MULTIPLE (but works also for SINGLE) {@link InputTons}
 * per Feed per load day.
 * BioSystem receiving just fresh inputs, so receiving a single input of one matrix in a single load day could revert to
 * {@link CSVOutputWriterSingleFreshInputTonsPerDayBehaviour} that labels files to account for this.
 * 
 * @author Marco Foi
 */
public class CSVOutputWriterMultipleInputTonsWDifferentAgesPerDayBehaviour extends CSVOutputWriterAbstractyBehaviour implements IOutputWriterBehavior {

	/**
	 * This suffix is used in exporting files inside InputBag export methods.
	 * 
	 * @return
	 */

	public String getSuffix() {
		return "MULTIPLEVALUES";
	}

	/**
	 * The data matrix is composed by:</br>
	 * <ul>
	 * <li>Row: the day of the year</li>
	 * <li>Column: the different InputTons available in the system in that day_of_year</li>
	 * <li>The value of the computed array for that InputTons: e.g.: the residual mass remaining of an InputTons on that
	 * day_of_year</li>
	 * </ul>
	 */
	public void saveYearMatrixToFile(double[][][] yearMatrix, String filePath) {
		
		try {
			File file = new File(filePath);

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			// System.out.println("NumRows: "+yearMatrix.length);
			System.out.println("Start dumping Year Matrix..");
			int lastPerc = 0;
			for (int row = 0; row < yearMatrix.length; row++) {
				// System.out.println("Row#: "+(row+1));
				String stringRow = "";
				String valueElement = "";
				String spacer = "";
				// System.out.println("NumCols: "+yearMatrix[row].length);

				Long start = System.currentTimeMillis();
				// System.out.println("Start row creation");

				for (int col = 0; col < yearMatrix[row].length; col++) {
					// System.out.println("Col#: "+(row+1));
					// #############################################################
					// Here the all values (corresponding to the input for each the InputTons
					// available for that loadDay) are returned
					// #############################################################
					// Loop in the different values from InputTons loaded on the same load day
					for (int inputTon = 0; inputTon < yearMatrix[row][col].length; inputTon++) {
						// stringRow += spacer + Double.toString(yearMatrix[row][col][inputTon]);
						// stringRow = stringRow.concat(spacer.concat(Double.toString(yearMatrix[row][col][inputTon])));
						valueElement = spacer.concat(df.format(yearMatrix[row][col][inputTon]));
						spacer = ",";
						bw.write(valueElement);
					}
				}
				// bw.write(stringRow+"\r\n");
				bw.newLine();

				if((((double)row)/((double)yearMatrix.length))*100 >= lastPerc + 10){
					lastPerc = lastPerc + 10;
					System.out.println(" "+lastPerc+"%");
					//System.out.println("YearDay " + (row + 1) + " dumped in: " + (System.currentTimeMillis() - start) + "ms");
				}
				// bw.flush();
			}
			System.out.println("100%");
			bw.close();

			System.out.println("Done! End writing file: "+filePath);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
