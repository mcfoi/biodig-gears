package eu.marcofoi.biodig.biodig_gears.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.InvalidParameterException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import eu.marcofoi.biodig.biodig_gears.BioSystem;

public class InputParser {

	/**
	 * Takes a .CSV file made by a column of 365 double values and parses it to an
	 * array. The doubles can be expressed with ordinary notation (123.43545435)
	 * or with scientific notation (1.23456E-102). Both "." and "," are
	 * acceptable decimal separators.
	 * 
	 * @param stringUrlToFile
	 *            A path to a .csv file containing input data separated by \n\r
	 * @return double[] or {@link InvalidParameterException} if the file has not
	 *         365 rows.
	 */

	public static double[] parseSingleColumnCSVFileToDoubleArray(String stringUrlToFile) {

		double[] doubleArray = new double[365];

		BufferedReader br = null;
		try {

			String sCurrentLine;

			br = new BufferedReader(new FileReader(stringUrlToFile));

			int lines = 0;
			double currentLineDouble;
			NumberFormat nf = null;
			while ((sCurrentLine = br.readLine()) != null) {
				if (nf == null) {
					if (sCurrentLine.indexOf(".") > 0) {
						nf = NumberFormat.getInstance(Locale.ENGLISH);
					} else {
						nf = NumberFormat.getInstance(Locale.ITALIAN);
					}
				}

				Number n = nf.parse(sCurrentLine);
				currentLineDouble = n.doubleValue();
				// currentLineDouble = Double.parseDouble(sCurrentLine);
				doubleArray[lines] = currentLineDouble;
				lines++;
			}

			if (lines != 365) { throw new InvalidParameterException("The input file does not have 365 rows!"); }

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return doubleArray;
	}

	/**
	 * This method has the same purpose as the method {@link #parseSingleColumnCSVFileToDoubleArray(File)} (Takeing a
	 * .CSV file made by
	 * a column of 365 double values and parseing it to an array.) but starting from an InputStream.<br>
	 * It is used in place of {@link #parseSingleColumnCSVFileToDoubleArray(File)} whenever an access is required to
	 * files packaged in
	 * the .JAR file hosting the application code, as it happens for the .CSV files of experimental curves used to
	 * initialize new feeds when no specific ones are provided. Specifically, this method is used to process the
	 * InputStream returned from calls to Feed.class.getResourceAsStream() [as calling getResource() - returning an URL
	 * - does not work for resources in the .JAR application file.
	 * 
	 * @param is
	 *            InputStream
	 * @return
	 */
	public static double[] parseInputStreamToDoubleArray(InputStream is) {

		double[] doubleArray = new double[365];

		BufferedReader br = null;
		try {

			String sCurrentLine;

			br = new BufferedReader(new InputStreamReader(is));
			// br = new BufferedReader(new FileReader(stringUrlToFile));

			int lines = 0;
			double currentLineDouble;
			NumberFormat nf = null;
			while ((sCurrentLine = br.readLine()) != null) {
				if (nf == null) {
					if (sCurrentLine.indexOf(".") > 0) {
						nf = NumberFormat.getInstance(Locale.ENGLISH);
					} else {
						nf = NumberFormat.getInstance(Locale.ITALIAN);
					}
				}

				Number n = nf.parse(sCurrentLine);
				currentLineDouble = n.doubleValue();
				// currentLineDouble = Double.parseDouble(sCurrentLine);
				doubleArray[lines] = currentLineDouble;
				lines++;
			}

			if (lines != 365) { throw new InvalidParameterException("The input file does not have 365 rows!"); }

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return doubleArray;
	}

	/**
	 * Take a.CSV file made by multiple columns of 365 double values and parse it to an array of arrays. The doubles can
	 * be expressed with ordinary notation (123.43545435) or with scientific notation (1.23456E-102). Both "." and ","
	 * are acceptable decimal separators.</br>
	 * When not parsable values are found, Double.NaN is returned.
	 * 
	 * @param stringUrlToFile
	 *            A path to a .csv file containing input data separated by ","
	 *            or ";" and terminated by "\n\r"
	 * @return double[] or {@link InvalidParameterException} if the file has not
	 *         365 rows.
	 */
	public static double[][] parseMultiColumnCSVFileToDoubleArray(String stringUrlToFile) {

		double[][] arrayOfDoubleArrays = new double[BioSystem.computationLengthInDays][];

		BufferedReader br = null;
		try {

			String sCurrentLine;

			br = new BufferedReader(new FileReader(stringUrlToFile));

			int lines = 0;
			double currentItemDouble;
			NumberFormat nf = null;
			while ((sCurrentLine = br.readLine()) != null) {
				// Replaces any ";" with "," and "," with "." to have "." for decimal separator and "," for values
				// separator
				if (sCurrentLine.indexOf(";") > 0 && sCurrentLine.indexOf(",") > 0) {
					sCurrentLine = sCurrentLine.replaceAll(",", "\\.");
					sCurrentLine = sCurrentLine.replaceAll(";", ",");
				}

				String[] rowItems = sCurrentLine.split(",|;");

				double[] row = new double[rowItems.length];

				if (nf == null) {
					if (sCurrentLine.indexOf(".") > 0) {
						nf = NumberFormat.getInstance(Locale.ENGLISH);
					} else {
						nf = NumberFormat.getInstance(Locale.ITALIAN);
					}
				}
				String item = null;
				for (int i = 0; i < rowItems.length; i++) {
					// TODO Fix this for when not parsable values are found
					item = rowItems[i];
					Number n;
					try {
						n = nf.parse(rowItems[i]);
					} catch (ParseException e) {
						// When not parsable values are found, Double.NaN is returned
						n = Double.NaN;
					}
					currentItemDouble = n.doubleValue();
					row[i] = currentItemDouble;
				}
				// currentLineDouble = Double.parseDouble(sCurrentLine);
				arrayOfDoubleArrays[lines] = row;
				lines++;
			}

			if (lines != BioSystem.computationLengthInDays) { throw new InvalidParameterException("The input file does not have " + BioSystem.computationLengthInDays + " rows!"); }

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return arrayOfDoubleArrays;
	}

	/**
	 * This method is a wrapper of {@ling #parseSingleColumnCSVFileToDoubleArray(File)} and is aimed at parsing an array
	 * of doubles out
	 * of an input file made by 365 rows containing each one single string that can be parsed to a double. Tipically
	 * this method is used to read deeding diearies of matixes of the experimental curves.
	 * 
	 * @param fileURL
	 *            A file containing a single column of stringa parsable to double.
	 * @return double[] An array of 365 double elements
	 */
	public static double[] parseSingleColumnCSVFileToDoubleArray(URL fileURL) {
		File file = new File(fileURL.getFile());
		return parseSingleColumnCSVFileToDoubleArray(file);
	}

	/**
	 * This method is a wrapper of {@ling #parseSingleColumnCSVFileToDoubleArray(String)} and is aimed at parsing an
	 * array of doubles out of an input file made by 365 rows containing each one
	 * single string that can be parsed to a double. Tipically this method is used to read deeding diearies of matixes
	 * of the experimental curves.
	 * 
	 * @param fileURL
	 *            A file containing a single column of stringa parsable to double.
	 * @return double[] An array of 365 double elements
	 */
	public static double[] parseSingleColumnCSVFileToDoubleArray(File file) {
		String fileStringName = file.getAbsolutePath();
		return parseSingleColumnCSVFileToDoubleArray(fileStringName);
	}

	/**
	 * Returns a matrix of rows and columns from the parsing of the file pointed by the URL.</br>
	 * The method is actually just a wrapper around {@link #parseMultiColumnCSVFileToDoubleArray(File)}
	 * 
	 * @param fileURL
	 *            A file containing comma separated, double prsable values, with NaN as null-values placer.</br> The
	 *            structure is as follows (usually 365 rows, any number of cols):</br></br><code>
	 *            0.433905,NaN,NaN,NaN,NaN,NaN,..</br>
	 *            0.326592,0.409799,NaN,NaN,NaN,NaN,..</br>
	 *            0.259825,0.308448,0.369623,NaN,NaN,NaN,..</br>
	 *            ..</br></code>
	 * @return double[][] The first index being the row, the second the column
	 */
	public static double[][] parseMultiColumnCSVFileToDoubleArray(URL fileURL) {
		File file = new File(fileURL.getFile());
		return parseMultiColumnCSVFileToDoubleArray(file);
	}

	/**
	 * Returns a matrix of rows and columns from the parsing of the file pointed by the URL.</br>
	 * The method is actually just a wrapper around {@link #parseMultiColumnCSVFileToDoubleArray(String)}
	 * 
	 * @param fileURL
	 *            A file containing comma separated, double prsable values, with NaN as null-values placer.</br> The
	 *            structure is as follows (usually 365 rows, any number of cols):</br></br><code>
	 *            0.433905,NaN,NaN,NaN,NaN,NaN,..</br>
	 *            0.326592,0.409799,NaN,NaN,NaN,NaN,..</br>
	 *            0.259825,0.308448,0.369623,NaN,NaN,NaN,..</br>
	 *            ..</br></code>
	 * @return double[][] The first index being the row, the second the column
	 */
	private static double[][] parseMultiColumnCSVFileToDoubleArray(File file) {
		String fileStringName = file.getAbsolutePath();
		return parseMultiColumnCSVFileToDoubleArray(fileStringName);
	}
	
	

	/**
	 * Returns the number of columns in the passed file.
	 * @param file Object Accepted Object can be URL | File | String [as from URL.getFile()]
	 * @return int Number of columns in file
	 */
	public static int getCols(Object file) {
		RowCol rc = getRowCol(file);
		return rc.getCols();
	}
	
	/**
	 * Returns the number of rows in the passed file.
	 * @param file Object Accepted Object can be URL | File | String [as from URL.getFile()]
	 * @return int Number of columns in file
	 */
	public static int getRows(Object file) {
		RowCol rc = getRowCol(file);
		return rc.getRows();
	}

	/**
	 * Class for returning file matrix size in a compact and predicatable way.
	 * 
	 * @param file
	 *            A .csv file containing data in rows and columns. All rows are expected to have same number of values.
	 * @return RowCol
	 */
	private static RowCol getRowCol(Object file) {

		RowCol rowCol = null;

		if (file.getClass().isAssignableFrom(URL.class)) {

			String stringUrlToFile = ((URL) file).getFile();

			BufferedReader br = null;
			try {

				String sCurrentLine;

				FileReader fr = new FileReader(stringUrlToFile);

				br = new BufferedReader(fr);

				int rows = 0;
				int cols = 0;

				while ((sCurrentLine = br.readLine()) != null) {
					// Replaces any ";" with "," and "," with "." to have "." for decimal separator and "," for values
					// separator
					if (sCurrentLine.indexOf(";") > 0 && sCurrentLine.indexOf(",") > 0) {
						sCurrentLine = sCurrentLine.replaceAll(",", "\\.");
						sCurrentLine = sCurrentLine.replaceAll(";", ",");
					}

					String[] rowItems = sCurrentLine.split(",|;");

					if (cols == 0) {
						cols = rowItems.length;
					} else {
						if (cols != rowItems.length) { throw new IllegalArgumentException("File row " + rows + " has " + rowItems.length + " while prevous had " + cols + ".\r\nInput file must have all rows with same number of elements!"); }
					}

					rows++;
				}

				br.close();
				fr.close();
				
				InputParser ip = new InputParser();
				rowCol = ip.new RowCol(rows, cols);
				
			} catch (IOException e) {
				// br could not be closed
				e.printStackTrace();
			}
		}

		return rowCol;

	}

	public class RowCol {
		private int	rowsNumber;
		private int	colsNumber;

		/**
		 * Create a RowCol object passing the two values: rows and cols as integers.
		 * 
		 * @param rows
		 *            int Number of file rows;
		 * @param col
		 *            int Number of column rows;
		 */
		public RowCol(int rows, int cols) {
			this.rowsNumber = rows;
			this.colsNumber = cols;
		}

		public int getCols() {
			return this.colsNumber;
		}

		public int getRows() {
			return this.rowsNumber;
		}
	}
}
