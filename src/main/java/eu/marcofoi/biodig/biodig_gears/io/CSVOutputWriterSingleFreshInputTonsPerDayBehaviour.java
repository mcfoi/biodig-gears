package eu.marcofoi.biodig.biodig_gears.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import eu.marcofoi.biodig.biodig_gears.InputTons;

/**
 * Class suitable for exporting data from a BioSystem fed with ONE SINGLE {@link InputTons} per Feed per load day.
 * BioSystem receiving inputs from a previous stage, so receiving multiple inputs of different ages of the same matrix
 * in a single load day should revert to {@link CSVOutputWriterMultipleInputTonsWDifferentAgesPerDayBehaviour}.
 * 
 * @author Marco Foi
 */
public class CSVOutputWriterSingleFreshInputTonsPerDayBehaviour extends CSVOutputWriterAbstractyBehaviour implements IOutputWriterBehavior {

	/**
	 * This suffix is used in exporting files inside InputBag export methods.
	 * 
	 * @return
	 */

	public String getSuffix() {
		return "SINGLEVALUES";
	}

	/**
	 * The data matrix is composed by:</br>
	 * <ul>
	 * <li>Row: the day of the year</li>
	 * <li>Column: the different InputTons available in the system in that day_of_year</li>
	 * <li>The value of the computed array for that InputTons: e.g.: the resudual mass remaining of an InputTons on that
	 * day_of_year</li>
	 * </ul>
	 */
	public void saveYearMatrixToFile(double[][][] yearMatrix, String filePath) {

		try {
			File file = new File(filePath);

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);

			for (int row = 0; row < yearMatrix.length; row++) {
				String stringRow = "";
				String spacer = "";
				for (int col = 0; col < yearMatrix[row].length; col++) {
					// #############################################################
					// Here the first value (corresponding to the first input for the
					// loadDay) is returned
					// #############################################################
					stringRow += spacer + df.format(yearMatrix[row][col][0]);
					spacer = ",";
				}
				bw.write(stringRow);
				bw.newLine();
			}
			bw.close();

			System.out.println("Done! End writing file: "+filePath);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
