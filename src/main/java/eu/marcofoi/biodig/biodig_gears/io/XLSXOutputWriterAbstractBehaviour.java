package eu.marcofoi.biodig.biodig_gears.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

public abstract class XLSXOutputWriterAbstractBehaviour implements IOutputWriterBehavior {

	public abstract double computeReturnedDailyValue(double dailyInputsArray[]);
	

	public String getOutputFileExtension() {
		return ".xlsx";
	}
	
	/**
	 * This suffix is used in exporting files inside InputBag export methods.
	 * @return
	 */

	public String getSuffix(){
		return "SUM-AND-SINGLEVALUES";
	}
	
	/**
	 * Saves a matrix of double data into a file using Apache POI library and
	 * specifically SXSS extensio to reduce memory footprint in writing a large
	 * .XLSX file of 365x365 cells. Also adds a row sum in first (A) column.
	 * 
	 * @param yearMatrix
	 *            the matrix
	 * @param filePath
	 *            the file path to the output file
	 * @return
	 */

	public void saveYearMatrixToFile(double[][][] yearMatrix, String filePath) {

		/*
		 * // Rows with rownum < 900 are flushed and not accessible for (int
		 * rownum = 0; rownum < 900; rownum++) {
		 * Assert.assertNull(sh.getRow(rownum)); }
		 * 
		 * // ther last 100 rows are still in memory for (int rownum = 900;
		 * rownum < 1000; rownum++) { Assert.assertNotNull(sh.getRow(rownum)); }
		 */

		SXSSFWorkbook wb = new SXSSFWorkbook(70); // keep 70 rows in memory,
		// exceeding rows will be
		// flushed to disk
		Sheet sh = wb.createSheet();

		for (int rownum = 0; rownum < yearMatrix.length; rownum++) {
			Row row = sh.createRow(rownum);
			double rowSum = 0.0;
			// Fill in data
			for (int colnum = 0; colnum < yearMatrix[rownum].length; colnum++) {

				Cell cell = row.createCell(colnum + 1, Cell.CELL_TYPE_NUMERIC);
				
				double value = computeReturnedDailyValue(yearMatrix[rownum][colnum]);
								
				if (!Double.isNaN(value)) {
					rowSum += value;
					cell.setCellValue(value);
				}
			}

			// Put SUM into first column
			Cell cell = row.createCell(0, Cell.CELL_TYPE_FORMULA);
			cell.setCellFormula("=SUM(B" + (rownum + 1) + ":NB" + (rownum + 1)
					+ ")");
			cell.setCellValue(rowSum);
		}

		FileOutputStream out;

		try {

			File file = new File(filePath);
			file.getParentFile().mkdirs();
			
			out = new FileOutputStream(filePath);

			wb.write(out);

			out.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Done! End writing file: "+filePath);
		
		// dispose of temporary files backing this workbook on disk
		wb.dispose();
	}

}
