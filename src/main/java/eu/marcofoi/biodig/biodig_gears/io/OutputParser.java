package eu.marcofoi.biodig.biodig_gears.io;

public class OutputParser {

	private IOutputWriterBehavior	outputParserBehavior;

	public OutputParser(IOutputWriterBehavior outputParserBehavior) {
		this.outputParserBehavior = outputParserBehavior;
	}

	/**
	 * Pass the data matrix for esport to the IOutputWriterBehavior
	 * 
	 * @param yearMatrix
	 * @param filePath
	 *            A file pat (path + filename-with-extension) RELATIVE to the running dir (e.g.: "./filname.csv")
	 */
	public void saveYearMatrixToFile(double[][][] yearMatrix, String filePath) {
		this.outputParserBehavior.saveYearMatrixToFile(yearMatrix, filePath);
	}
}
