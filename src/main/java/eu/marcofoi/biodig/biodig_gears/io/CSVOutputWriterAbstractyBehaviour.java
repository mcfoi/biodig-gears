package eu.marcofoi.biodig.biodig_gears.io;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import eu.marcofoi.biodig.biodig_gears.utils.Utils;

/**
 * This is the parent class of all OutputWriter exporting in .csv format.
 * It is used to control a number of aspects that must be coherent across all those writers:
 * 
 * @author Marco Foi
 */
public abstract class CSVOutputWriterAbstractyBehaviour {

	/**
	 * This DecimalFormat controls how the computed values are truncated when written in the output file.
	 * Such truncation is introduced to limit the total size of the saved files obtained from the direct export of
	 * doubles with all decimals, using Double.toString().
	 * A number of 6 decimals ("#.######") seems good to express values of residual tons. It might be increased if
	 * required.
	 */
	private DecimalFormatSymbols	dfs				= new DecimalFormatSymbols(Locale.US);
	public DecimalFormat			df				= null;

	/**
	 * String to be used in the sub-calsses to define the encoding of the implemented FilOutputStrams:</br>
	 * E.g.: new FileOutputStream(fileDir), FILE_ENCODING));
	 */

	public final String				FILE_ENCODING	= "UTF8";

	public CSVOutputWriterAbstractyBehaviour() {

		dfs.setNaN("NaN");
		dfs.setInfinity("infinity");
		df = Utils.getDecimalFormat("0.000000", dfs);

	}

	/**
	 * Returns the extension to be used for naming the exported files.
	 * 
	 * @return
	 */
	public String getOutputFileExtension() {
		return ".csv";
	}

}
