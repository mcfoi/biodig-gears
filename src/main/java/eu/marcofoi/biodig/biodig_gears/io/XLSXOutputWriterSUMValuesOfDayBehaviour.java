package eu.marcofoi.biodig.biodig_gears.io;

import eu.marcofoi.biodig.biodig_gears.feeds.Feed;

public class XLSXOutputWriterSUMValuesOfDayBehaviour extends
		XLSXOutputWriterAbstractBehaviour {

	/**
	 * This method takes as parameter an array containing all values calculated
	 * for a single loadDay and implements the logic required to return just one
	 * double. This is needed when are being dumped to file matrices coming from
	 * a system composed by multiple stages where in a single loadDay multiple
	 * inputs of the same {@link Feed} are present hence there is not just one
	 * value in the data matrix. Given that Excel cannot support infinite
	 * columns (just 16000 in Ex2010) some kind of logic must be implemented to
	 * return one single value from all values computed for the feeds of the
	 * same type in the same day. In the case of "SUMValuesOfDay" (this) implementation
	 * a sum of all values for the loadDay is returned.
	 */
	public double computeReturnedDailyValue(double[] dailyInputsArray) {
		
		double sum = 0;
		
		for(int i = 0; i < dailyInputsArray.length; i++){
			sum += dailyInputsArray[i];
		}
		
		return sum;
	}
	
	/**
	 * This suffix is used in exporting files inside InputBag export methods.
	 * @return
	 */
	@Override
	public String getSuffix(){
		return "SUM-AND-SINGLEVALUES";
	}
}
