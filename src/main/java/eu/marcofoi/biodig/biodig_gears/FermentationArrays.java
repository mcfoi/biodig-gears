package eu.marcofoi.biodig.biodig_gears;

/**
 * This Enum contains a list of all possible parameters made available by the
 * {@link FermentationCalculusLogic} after its executions over an
 * {@link InputBag} and all its contained {@link InputTons}
 * 
 * @author mcfoi
 * 
 */
public enum FermentationArrays implements ICalculatedArrays {
	RESIDUAL_MASS_YEAR_ARRAY, DAILY_OUTPUT_MASS_YEAR_ARRAY, TSS_LOST_YEAR_ARRAY;
}
