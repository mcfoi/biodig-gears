package eu.marcofoi.biodig.biodig_gears;

import java.util.HashMap;

import eu.marcofoi.biodig.biodig_gears.feeds.Feed;

public class InputTons {

	private double						startTons;
	private int							startAge			= 1;
	private Feed						feedType;
	private int							loadDay;
	private ICalculusLogic				calculusLogic;
	private BioSystem					bioSystem;
	// private double[] dailyOutputMassYearArray;
	// private double[] residualMassYearArray;
	// private double[] tss_lossYearArray;

	private HashMap<Enum<?>, double[]>	yearArraysMap;

	private boolean						isCalcLogicExecuted	= false;

	/**
	 * This class represents in the software the total daily mass added to the system of one Feed type (e.g.: 'maize').
	 * The implemented logic does not allow for two feeds of the same type to be added to the system for the same day.
	 * This specific constructor must be used to represent <b>just fresh contributes</b> added to the first stage of the
	 * {@link BioSystem} as it just calls the more general
	 * {@link InputTons#InputTons(BioSystem, double, Feed, int, int)} constructor that allow to specify a fifth
	 * parameter that reports how many days has the InputTons passed in the BioSystem - for a correct alculation of mass
	 * loss.
	 * 
	 * @param bioSystem
	 *            The BioSystem in which the InputTons has been added
	 * @param startTons
	 *            The mass off the contribute to the BioSystem
	 * @param feedType
	 *            The type of feed (as each feed type has a specific empirical curve required in ICalculusLogic)
	 * @param loadDay
	 *            The day of the year (0 based: from 0 to 364) in which the input has been added to the system: it is
	 *            needed to define from which
	 *            date the InputTons starts contributing to the BioSystem with gas emissions
	 */
	public InputTons(BioSystem bioSystem, double startTons, Feed feedType, int loadDay) {
		this(bioSystem, startTons, feedType, loadDay, 1);
	}

	/**
	 * This class represents in the software the total daily mass added to the
	 * system of one Feed type (e.g.: 'maize'). The implemented logic does not
	 * allow for two feeds of the same type to be added to the system for the
	 * same day.
	 * 
	 * @param bioSystem
	 *            The BioSystem in which the InputTons has been added
	 * @param startTons
	 *            The mass of the contribute to the BioSystem
	 * @param feedType
	 *            The type of feed (each feed type has specific empirical curves required in ICalculusLogic for
	 *            computing mass loss and GAS not produced)
	 * @param loadDay
	 *            The day of the year( 0 based: from 0 to 364) in which the input has been added to the system: in
	 *            needed to define from which
	 *            date it starts contributing to the BioSystem with gas emissions
	 * @param startAge
	 *            The age of the input in days. Must be 1 for fresh masses added to the first stage of a multi-stage
	 *            BioSystem while represents the total number of days already spent by the InputTons in all previous
	 *            stages stages of the BioSystem for not-fresh contributes.</br>This is used to select the right index
	 *            in the array of the experimental curve represented by the FeedType TSS_LOST_on_TTQ array from which to
	 *            start the computation of the mass lost day by day. Otherwise the input would act as a fresh material
	 *            even though it is just passing from a stage of the system to another.
	 */
	public InputTons(BioSystem bioSystem, double startTons, Feed feedType, int loadDay, int startAge) {
		super();
		this.bioSystem = bioSystem;
		this.startTons = startTons;
		this.feedType = feedType;
		this.loadDay = loadDay;
		this.startAge = startAge;
	}

	public double getStartTons() {
		return this.startTons;
	}

	public Feed getFeedtype() {
		return feedType;
	}

	public void setFeedType(Feed feedType) {
		this.feedType = feedType;
	}

	public int getLoadDay() {
		return this.loadDay;
	}

	public int getStartAge() {
		return startAge;
	}

	public void setStartAge(int updatedAge) {
		this.startAge = updatedAge;
	}

	public void setCalculusLogic(ICalculusLogic calculusLogic) {
		this.calculusLogic = calculusLogic;
	}

	public ICalculusLogic getCalculusLogic() {
		return this.calculusLogic;
	}

	public BioSystem getBioSystem() {
		return bioSystem;
	}

	public void setBioSystem(BioSystem bioSystem) {
		this.bioSystem = bioSystem;
	}

	/**
	 * This method triggers the execution of the calculation logic composed with
	 * the InputTons and cause all data describing the life of the InputTons in
	 * the BioSystem to be calculated.
	 */
	public void executeCalcLogic() {

		if (isCalcLogicExecuted == false) {

			// double[][] yearArrays =
			// calculusLogic.calculateFermentationArrays(this.startTons);

			/**
			 * The result will be an array of arrays, each of which is one of the quantities defined
			 * by the {@link ICalculatedArrays} used in the implemented {@link ICalculusLogic}.</br>
			 * These can be, in the case of {@link FermentationArrays}:
			 */
			// residualMassYearArray = yearArrays[0];
			// dailyOutputMassYearArray = yearArrays[1];
			// tss_lossYearArray = yearArrays[2];

			yearArraysMap = calculusLogic.calculateFermentationArrays(this);

			isCalcLogicExecuted = true;
		}
	}

	/**
	 * Returns the value from the array named 'e' for the requested day_of_year.
	 * 
	 * @param day_of_year
	 * @param e
	 *            One of the Enum<?> of the {@link ICalculatedArrays} composed
	 *            with the {@link ICalculusLogic} associated with the InputTons.
	 * @return double A value computed with the {@link ICalculusLogic} on the
	 *         current InputTons.
	 */
	public double getValueFromCalculatedArray(int day_of_year, Enum<?> e) {

		if (isCalcLogicExecuted == true) {
			if (day_of_year < 1 || day_of_year > 365) {
				throw new UnsupportedOperationException("The requested day_of_year must be in the [1-365] range.");
			} else {

				// Get the array of the specific computed parameter
				double[] yearlyArray = yearArraysMap.get(e);

				int arrayIndex = day_of_year - getLoadDay();
				if (arrayIndex < 0) {
					// The inspected input was not jet in the system on the
					// requested day_of_year
					return Double.NaN;
				} else {

					return yearlyArray[arrayIndex];
				}
			}
		} else {
			throw new UnsupportedOperationException("The queried InputTons has not already been processed.");
		}
	}

	@Override
	public String toString() {
		return "Tons: " + Double.toString(startTons) + " Load Day:" + Integer.toString(loadDay) + " Start Age:" + Integer.toString(startAge);
	}

	/**
	 * Returns the Residual Mass in tons of the input in the given day. This
	 * quantity represents how much mass of the InputTons is still inside the
	 * system at the requested day_of_year. The value decreases with the
	 * increasing time spent by the input in the system due to two causes: the
	 * mechanical daily removal from the system of a slightly varying volume
	 * amount (done by system managers as routine activity, expressed in units
	 * of volume: m^3) and the natural loss of mass due to chemical reactions
	 * that lead to natural gas emissions.
	 * 
	 * @param day_of_year
	 *            The day of the year: ranges from 1 to 365
	 * @return the Residual Volume value or Double.NaN if the inspected input
	 *         was not jet in the system on the requested day_of_year
	 */
	/*
	 * public double getResidualMass(int day_of_year) {
	 * if (isCalcLogicExecuted == true) { if (day_of_year < 1 || day_of_year >
	 * 365) { throw new UnsupportedOperationException(
	 * "The requested day_of_year must be in the [1-365] range."); } else { int
	 * arrayIndex = day_of_year - getLoadDay(); if (arrayIndex < 0) { // The
	 * inspected input was not jet in the system on the // requested day_of_year
	 * return Double.NaN; } else { return
	 * this.residualMassYearArray[arrayIndex]; } } } else { throw new
	 * UnsupportedOperationException(
	 * "The queried InputTons has not already been processed."); } }
	 */

	/**
	 * Returns the Daily Output in tons of the input for the given day. This
	 * quantity represents how much mass of the InputTons has left the system in
	 * the requested day_of_year, due to the removal from the system of the
	 * daily volume out-take, as recorded in the real system story board and
	 * added to the software simulator at initialization of BioSystem with the
	 * "totalOutputM3CSVFile" .csv file. This Daily Output considers the
	 * dilution of the InputTons into the whole volume of the system and also
	 * any previous removal form the system in all days preceding the
	 * day_of_year requested. Hence, being the initial InputTons volume any day
	 * depleted of a quantity, it will be more diluted so the Daily Output will
	 * progressively diminish.
	 * 
	 * @param day_of_year
	 *            The day of the year: ranges from 1 to 365
	 * @return the Daily Output value in tons or Double.NaN if the inspected
	 *         input was not jet in the system on the requested day_of_year.
	 */
	/*
	 * public double getDailyOutput(int day_of_year) {
	 * if (isCalcLogicExecuted == true) { if (day_of_year < 1 || day_of_year >
	 * 365) { throw new UnsupportedOperationException(
	 * "The requested day_of_year must be in the [1-365] range."); } else { int
	 * arrayIndex = day_of_year - getLoadDay(); if (arrayIndex < 0) { // The
	 * inspected input was not jet in the system on the // requested day_of_year
	 * return Double.NaN; } else { return
	 * this.dailyOutputMassYearArray[arrayIndex]; } } } else { throw new
	 * UnsupportedOperationException(
	 * "The queried InputTons has not already been processed."); } }
	 */

	/**
	 * Returns the TSS loss of the input for the given day, This quantity is
	 * computed starting from the Residual Mass and multiplying this value from
	 * another obtained from an empircal curve that is specific for each Feed
	 * type (e.g.: 'maize' has its curve). Such value express the loss of the
	 * specific feed type due to .................... TODO: ask for details
	 * 
	 * @param day_of_year
	 *            The day of the year: ranges from 1 to 365
	 * @return the TSS loss value or Double.NaN if the inspected input was not
	 *         jet in the system on the requested day_of_year
	 */
	/*
	 * public double getTSSloss(int day_of_year) {
	 * if (isCalcLogicExecuted == true) { if (day_of_year < 1 || day_of_year >
	 * 365) { throw new UnsupportedOperationException(
	 * "The requested day_of_year must be in the [1-365] range."); } else { int
	 * arrayIndex = day_of_year - getLoadDay(); if (arrayIndex < 0) { // The
	 * inspected input was not jet in the system on the // requested day_of_year
	 * return Double.NaN; } else { return this.tss_lossYearArray[arrayIndex]; }
	 * } } else { throw new UnsupportedOperationException(
	 * "The queried InputTons has not already been processed."); } }
	 */
}