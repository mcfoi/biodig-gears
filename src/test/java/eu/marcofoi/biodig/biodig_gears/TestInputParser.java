package eu.marcofoi.biodig.biodig_gears;

import static org.junit.Assert.assertEquals;

import java.net.URL;

import org.junit.Before;
import org.junit.Test;

import eu.marcofoi.biodig.biodig_gears.feeds.Feed;
import eu.marcofoi.biodig.biodig_gears.feeds.MaisFeed;
import eu.marcofoi.biodig.biodig_gears.io.InputParser;
import eu.marcofoi.biodig.biodig_gears.io.InputParser.RowCol;

public class TestInputParser {

	double[] da;
	private String testMaizeFeedsFile;
	private double[] feedsArray;
	private URL	urlMultiColumnCSV;
	private URL	urlTSS_MAIS_CSV;

	@Before
	public void setUp() throws Exception {

		// Pick test file from /src/test/resources/eu/marcofoi/biodig/biodig_gears/feeds/
		// Path is relative to root as test run as being started from project root 
		urlTSS_MAIS_CSV = this.getClass().getResource("./feeds/"+ Feed.TSS_FILENAME_PREFIX + Feed.SPACER + MaisFeed.FEED_NAME + ".csv");
		// Path is relative to root as test run as being started from project root 
		// Pick test file from /src/test/resources/eu/marcofoi/biodig/biodig_gears/
		urlMultiColumnCSV = this.getClass().getResource("BS_MAIS_DAILY_OUTPUT_MASS_YEAR_ARRAY_SINGLEVALUES.csv");
		
		// Convert from URL to path
		String testTotalOutputM3File = urlTSS_MAIS_CSV.getPath();
		// Parse file
		da = InputParser
				.parseSingleColumnCSVFileToDoubleArray(testTotalOutputM3File);
	}

	@Test
	public void testCountParsedDoubleArray() {
		assertEquals(365, da.length);
	}

	@Test
	public void testParseFileToDoubleArray() {
		assertEquals(0.229110658, da[0], 0.00000001);
		assertEquals(1.1086E-166, da[da.length - 1], 0.00000001);
	}

	@Test
	public void testInputFeeds() {

		// Pick test file from /src/test/resources/eu/marcofoi/biodig_gears/
		//URL url = this.getClass().getResource("FEED_MAIS.csv");
		URL url = this.getClass().getResource(Feed.FEED_FILENAME_PREFIX + Feed.SPACER + MaisFeed.FEED_NAME + ".csv");
		// Convert from URL to path
		testMaizeFeedsFile = url.getPath();

		feedsArray = InputParser
				.parseSingleColumnCSVFileToDoubleArray(testMaizeFeedsFile);

		assertEquals(27.0, feedsArray[0], 0.05);
		assertEquals(25.5, feedsArray[1], 0.05);
		assertEquals(27.0, feedsArray[364], 0.05);
	}
	
	@Test
	public void testInputFeedsMultiColumn() {

		// Pick test file from /src/test/resources/eu/marcofoi/biodig_gears/
		URL url = this.getClass().getResource("BS_MAIS_DAILY_OUTPUT_MASS_YEAR_ARRAY_SINGLEVALUES.csv");
		// Convert from URL to path
		String testFeedsFileMultiColumns = url.getPath();

		double[][] feedsArrayMultiColumn = InputParser
				.parseMultiColumnCSVFileToDoubleArray(testFeedsFileMultiColumns);

		assertEquals(0.433905, feedsArrayMultiColumn[0][0], 0.0005);
		assertEquals(0.326592, feedsArrayMultiColumn[1][0], 0.0005);
		assertEquals(0.409799, feedsArrayMultiColumn[1][1], 0.0005);
		assertEquals(0.001231, feedsArrayMultiColumn[364][0], 0.0005);
		assertEquals(0.433905, feedsArrayMultiColumn[364][364], 0.0005);
	}
	
	
	
	@Test
	public void setParseMultiColumn() throws Exception {

		// Pick test file from /src/test/resources/eu/marcofoi/biodig_gears/
		URL url = this.getClass().getResource("MULTIFEED_MAIS.csv");
		// Convert from URL to path
		String testTotalOutputM3File = url.getPath();
		// Parse file
		double[][] mda = InputParser
				.parseMultiColumnCSVFileToDoubleArray(testTotalOutputM3File);
		
		assertEquals(365, mda.length);
		assertEquals(3, mda[0].length);
		assertEquals(3, mda[50].length);
		assertEquals(3, mda[100].length);
		assertEquals(3, mda[364].length);
		assertEquals(27.0, mda[0][0], 0.05);
		assertEquals(22.0, mda[49][0], 0.05);
		assertEquals(18.0, mda[99][0], 0.05);
		assertEquals(27.0, mda[364][0], 0.05);
		assertEquals(16.0, mda[0][1], 0.05);
		assertEquals(21.0, mda[49][1], 0.05);
		assertEquals(25.0, mda[99][1], 0.05);
		assertEquals(15.0, mda[364][1], 0.05);
		assertEquals(17.0, mda[0][2], 0.05);
		assertEquals(25.0, mda[49][2], 0.05);
		assertEquals(26.0, mda[99][2], 0.05);
		assertEquals(19.0, mda[364][2], 0.05);
	}

	@Test
	public void setParseMultiColumnSemicolon() throws Exception {

		// Pick test file from /src/test/resources/eu/marcofoi/biodig_gears/
		URL url = this.getClass().getResource("MULTIFEEDsemicolon_MAIS.csv");
		// Convert from URL to path
		String testTotalOutputM3File = url.getPath();
		// Parse file
		double[][] mda = InputParser
				.parseMultiColumnCSVFileToDoubleArray(testTotalOutputM3File);
		
		assertEquals(365, mda.length);
		assertEquals(3, mda[0].length);
		assertEquals(3, mda[50].length);
		assertEquals(3, mda[100].length);
		assertEquals(3, mda[364].length);
		assertEquals(27.0, mda[0][0], 0.05);
		assertEquals(22.0, mda[49][0], 0.05);
		assertEquals(18.0, mda[99][0], 0.05);
		assertEquals(27.0, mda[364][0], 0.05);
		assertEquals(16.0, mda[0][1], 0.05);
		assertEquals(21.0, mda[49][1], 0.05);
		assertEquals(25.0, mda[99][1], 0.05);
		assertEquals(15.0, mda[364][1], 0.05);
		assertEquals(17.0, mda[0][2], 0.05);
		assertEquals(25.0, mda[49][2], 0.05);
		assertEquals(26.0, mda[99][2], 0.05);
		assertEquals(19.0, mda[364][2], 0.05);
	}
	
	@Test
	public void setParseMultiColumnSemicolonColon() throws Exception {

		// Pick test file from /src/test/resources/eu/marcofoi/biodig_gears/
		URL url = this.getClass().getResource("MULTIFEEDsemicolon_colon_MAIS.csv");
		// Convert from URL to path
		String testTotalOutputM3File = url.getPath();
		// Parse file
		double[][] mda = InputParser
				.parseMultiColumnCSVFileToDoubleArray(testTotalOutputM3File);
		
		assertEquals(365, mda.length);
		assertEquals(3, mda[0].length);
		assertEquals(3, mda[50].length);
		assertEquals(3, mda[100].length);
		assertEquals(3, mda[364].length);
		assertEquals(27.0, mda[0][0], 0.05);
		assertEquals(22.0, mda[49][0], 0.05);
		assertEquals(18.0, mda[99][0], 0.05);
		assertEquals(27.0, mda[364][0], 0.05);
		assertEquals(16.0, mda[0][1], 0.05);
		assertEquals(21.0, mda[49][1], 0.05);
		assertEquals(25.0, mda[99][1], 0.05);
		assertEquals(15.0, mda[364][1], 0.05);
		assertEquals(17.0, mda[0][2], 0.05);
		assertEquals(25.0, mda[49][2], 0.05);
		assertEquals(26.0, mda[99][2], 0.05);
		assertEquals(19.0, mda[364][2], 0.05);
	}
	
	@Test
	public void testGetRowCol(){
		
		
		assertEquals(365, InputParser.getRows(urlTSS_MAIS_CSV));
		assertEquals(1, InputParser.getCols(urlTSS_MAIS_CSV));
		
		assertEquals(365, InputParser.getRows(urlMultiColumnCSV));
		assertEquals(365, InputParser.getCols(urlMultiColumnCSV));
		
	}
}
