package eu.marcofoi.biodig.biodig_gears;

import static org.junit.Assert.assertEquals;

import java.net.URL;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import eu.marcofoi.biodig.biodig_gears.BioSystem;
import eu.marcofoi.biodig.biodig_gears.FermentationCalculusLogic;
import eu.marcofoi.biodig.biodig_gears.ICalculusLogic;
import eu.marcofoi.biodig.biodig_gears.InputBag;
import eu.marcofoi.biodig.biodig_gears.InputTons;
import eu.marcofoi.biodig.biodig_gears.feeds.Feed;
import eu.marcofoi.biodig.biodig_gears.feeds.MaisFeed;
import eu.marcofoi.biodig.biodig_gears.io.CSVOutputWriterSingleFreshInputTonsPerDayBehaviour;
import eu.marcofoi.biodig.biodig_gears.io.InputParser;

public class TestInputBag {

	private ICalculusLogic calculusLogic;
	private MaisFeed maizeFeedType;
	private InputBag bag;
	private BioSystem bs;
	private URL totalOutputM3CSVFile;

	@Before
	public void setUp() throws Exception {

		// Pick fermentation curve file from /src/test/resources/eu/marcofoi/biodig_gears/feeds
		// Path is relative to root as test run as being started from project root 
		URL fermentationCurveFileURL = this.getClass().getResource("./feeds/"+ Feed.TSS_FILENAME_PREFIX + Feed.SPACER + MaisFeed.FEED_NAME + ".csv");

		maizeFeedType = new MaisFeed(fermentationCurveFileURL);

		// This is required to define the system initial status
		totalOutputM3CSVFile = this.getClass().getResource("TOT_OUTPUT_M3.csv");

		bs = new BioSystem(2550, totalOutputM3CSVFile);

		calculusLogic = new FermentationCalculusLogic(bs);

		bag = new InputBag(bs, maizeFeedType, calculusLogic);

	}

	@Test
	public void testGetBagType() {
		assertEquals(MaisFeed.class, bag.getBagType().getClass());
	}

	@Test
	public void testMultiplePutInputTons() {

		// Load inputs into system
		// URL url = this.getClass().getResource("FEED_MAIS.csv");
		URL url = this.getClass().getResource(Feed.FEED_FILENAME_PREFIX + Feed.SPACER + MaisFeed.FEED_NAME + ".csv");
		
		double[] feedsArray = InputParser.parseSingleColumnCSVFileToDoubleArray(url);

		ICalculusLogic calculusLogic = new FermentationCalculusLogic(bs);
		
		bag.clear();

		for (int i = 0; i < feedsArray.length; i++) {
			bag.put(new InputTons(bs, feedsArray[i], maizeFeedType,	i + 1));		
		}

		assertEquals(365, bag.size());
	}

	@Test
	public void testPutInputTons() {
		InputTons input = new InputTons(bs, 27.0, maizeFeedType, 1);

		bag.clear();
		bag.put(input);

		assertEquals(1, bag.size());
	}

	@Ignore
	@Test(expected = UnsupportedOperationException.class)
	public void testUnsupportedOperationException() {
		InputTons input = new InputTons(bs, 27.0, maizeFeedType, 1);

		bag.put(input);

		assertEquals(1, bag.size());

		CSVOutputWriterSingleFreshInputTonsPerDayBehaviour wb = new CSVOutputWriterSingleFreshInputTonsPerDayBehaviour();

		// This throws an exception since no bag.processInputs() has ever been called
		bag.exportComputation(wb);
	}

}
