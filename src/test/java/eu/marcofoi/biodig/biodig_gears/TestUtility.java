package eu.marcofoi.biodig.biodig_gears;

import static org.junit.Assert.*;

import java.net.URL;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import eu.marcofoi.biodig.biodig_gears.feeds.Feed;
import eu.marcofoi.biodig.biodig_gears.feeds.LiquameBovinoFeed;
import eu.marcofoi.biodig.biodig_gears.feeds.LoiettoFeed;
import eu.marcofoi.biodig.biodig_gears.feeds.MaisFeed;
import eu.marcofoi.biodig.biodig_gears.feeds.PastoneDiMaisFeed;
import eu.marcofoi.biodig.biodig_gears.feeds.PollinaFeed;
import eu.marcofoi.biodig.biodig_gears.utils.Utils;

public class TestUtility {

	URL	maizeFeedsFileURL;
	URL	loiettoFeedsFileURL;
	URL	pollinaFeedsFileURL;
	URL	liquamebovinoFeedsFileURL;
	URL	pastonedimaisFeedsFileURL;

	@Before
	public void setUp() throws Exception {

		maizeFeedsFileURL = this.getClass().getResource(Feed.FEED_FILENAME_PREFIX + Feed.SPACER + MaisFeed.FEED_NAME + ".csv");
		loiettoFeedsFileURL = this.getClass().getResource(Feed.FEED_FILENAME_PREFIX + Feed.SPACER + LoiettoFeed.FEED_NAME + ".csv");
		pollinaFeedsFileURL = this.getClass().getResource(Feed.FEED_FILENAME_PREFIX + Feed.SPACER + PollinaFeed.FEED_NAME + ".csv");
		liquamebovinoFeedsFileURL = this.getClass().getResource(Feed.FEED_FILENAME_PREFIX + Feed.SPACER + LiquameBovinoFeed.FEED_NAME + ".csv");
		pastonedimaisFeedsFileURL = this.getClass().getResource(Feed.FEED_FILENAME_PREFIX + Feed.SPACER + PastoneDiMaisFeed.FEED_NAME + ".csv");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void TestgetFeedClassFromString() {

		assertTrue(MaisFeed.class.isAssignableFrom(Feed.getFeedClassFromString("MAIS")));
		assertTrue(MaisFeed.class.isAssignableFrom(Feed.getFeedClassFromString(MaisFeed.FEED_NAME)));
		
		assertTrue(LoiettoFeed.class.isAssignableFrom(Feed.getFeedClassFromString("LOIETTO")));
		assertTrue(LoiettoFeed.class.isAssignableFrom(Feed.getFeedClassFromString(LoiettoFeed.FEED_NAME)));
		
		assertTrue(PollinaFeed.class.isAssignableFrom(Feed.getFeedClassFromString("POLLINA")));
		assertTrue(PollinaFeed.class.isAssignableFrom(Feed.getFeedClassFromString(PollinaFeed.FEED_NAME)));
		
		assertTrue(PastoneDiMaisFeed.class.isAssignableFrom(Feed.getFeedClassFromString("PASTONE-DI-MAIS")));
		assertTrue(PastoneDiMaisFeed.class.isAssignableFrom(Feed.getFeedClassFromString(PastoneDiMaisFeed.FEED_NAME)));
		
		assertTrue(LiquameBovinoFeed.class.isAssignableFrom(Feed.getFeedClassFromString("LIQUAME-BOVINO")));
		assertTrue(LiquameBovinoFeed.class.isAssignableFrom(Feed.getFeedClassFromString(LiquameBovinoFeed.FEED_NAME)));
		
		assertNull(Feed.getFeedClassFromString("maize"));

	}

	@Test
	public void getFeedFromFilenameURL() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {

		assertTrue(Feed.class.isAssignableFrom(Feed.getFeedFromFilenameURL(maizeFeedsFileURL).getClass()));
		assertTrue(MaisFeed.class.isAssignableFrom(Feed.getFeedFromFilenameURL(maizeFeedsFileURL).getClass()));

		assertTrue(Feed.class.isAssignableFrom(Feed.getFeedFromFilenameURL(loiettoFeedsFileURL).getClass()));
		assertTrue(LoiettoFeed.class.isAssignableFrom(Feed.getFeedFromFilenameURL(loiettoFeedsFileURL).getClass()));

		assertTrue(Feed.class.isAssignableFrom(Feed.getFeedFromFilenameURL(pollinaFeedsFileURL).getClass()));
		assertTrue(PollinaFeed.class.isAssignableFrom(Feed.getFeedFromFilenameURL(pollinaFeedsFileURL).getClass()));

		assertTrue(Feed.class.isAssignableFrom(Feed.getFeedFromFilenameURL(liquamebovinoFeedsFileURL).getClass()));
		assertTrue(LiquameBovinoFeed.class.isAssignableFrom(Feed.getFeedFromFilenameURL(liquamebovinoFeedsFileURL).getClass()));

		assertTrue(Feed.class.isAssignableFrom(Feed.getFeedFromFilenameURL(pastonedimaisFeedsFileURL).getClass()));
		assertTrue(PastoneDiMaisFeed.class.isAssignableFrom(Feed.getFeedFromFilenameURL(pastonedimaisFeedsFileURL).getClass()));

		assertFalse(LoiettoFeed.class.isAssignableFrom(Feed.getFeedFromFilenameURL(maizeFeedsFileURL).getClass()));

	}

}
