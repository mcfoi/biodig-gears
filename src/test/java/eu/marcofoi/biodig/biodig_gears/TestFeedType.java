package eu.marcofoi.biodig.biodig_gears;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;

import org.junit.Before;
import org.junit.Test;

import eu.marcofoi.biodig.biodig_gears.feeds.Feed;
import eu.marcofoi.biodig.biodig_gears.feeds.MaisFeed;

public class TestFeedType {

	Feed ft;

	@Before
	public void setUp() throws Exception {

		// Path is relative to root as test run as being started from project root 
		URL maizeFeeds = this.getClass().getResource("./feeds/"+ Feed.TSS_FILENAME_PREFIX + Feed.SPACER + MaisFeed.FEED_NAME + ".csv");
		ft = new MaisFeed(maizeFeeds);

	}

	@Test
	public void testToStringFeedNameOutput() {
		assertEquals("MAIS", ft.toString());
	}

	@Test
	public void testGetTSSLostOnTTQ_on_day_of_year() {
		assertEquals(0.229110658, ft.getTSSLostOnTTQ_on_day_of_year(1),
				0.00000001);
		assertEquals(1.1086E-166, ft.getTSSLostOnTTQ_on_day_of_year(365),
				0.00000001);
	}

	@Test
	/**
	 * Check the array computed is of the right length of 365 elements and all elements are not null
	 */
	public void testArrayNotNull365Elements() {
		assertEquals(365, ft.getTSSLoss_on_TTQ_Array().length);

		// All elements are not null
		for (int i = 0; i < ft.getTSSLoss_on_TTQ_Array().length; i++) {
			assertNotNull(ft.getTSSLoss_on_TTQ_Array()[i]);
		}
	}
	
	@Test
	/**
	 * Check the array: all elements are of decreasing value
	 */
	public void testArrayIsMonotonicDecreasing() {
		// All elements are not null
		for (int i = 0; i < ft.getTSSLoss_on_TTQ_Array().length-1; i++) {
			assertTrue(ft.getTSSLoss_on_TTQ_Array()[i]> ft.getTSSLoss_on_TTQ_Array()[i+1]);
		}
	}

}
