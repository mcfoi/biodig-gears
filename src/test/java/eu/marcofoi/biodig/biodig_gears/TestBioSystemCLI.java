package eu.marcofoi.biodig.biodig_gears;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import eu.marcofoi.biodig.biodig_gears.feeds.Feed;
import eu.marcofoi.biodig.biodig_gears.feeds.MaisFeed;

public class TestBioSystemCLI {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testNOSecondStage() throws FileNotFoundException, IOException, IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		System.out.println("Starting Test SINGLE STAGE..\n\r");

		String feedfile = Feed.FEED_FILENAME_PREFIX + Feed.SPACER + MaisFeed.FEED_NAME + ".csv"; //"FEED_MAIS.csv";
		
		// While debugging in ECLIPSE, the host folder HAS to be added in "Debug Configurations -> Classpath"
		BioSystemCLI.main(new String[] { "2550.0", "TOT_OUTPUT_M3.csv", feedfile, "false" });
		
		System.out.println("\n\rTest is over. Closing!");

	}
	
	@Test
	public void testWithSecondStage() throws FileNotFoundException, IOException, IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		System.out.println("Starting Test TWO STAGES..\n\r");

		String feedfile = Feed.FEED_FILENAME_PREFIX + Feed.SPACER + MaisFeed.FEED_NAME + ".csv"; //"FEED_MAIS.csv";
		
		// While debugging in ECLIPSE, the host folder HAS to be added in "Debug Configurations -> Classpath"
		BioSystemCLI.main(new String[] { "2550.0", "TOT_OUTPUT_M3.csv", feedfile, "true" });
		
		System.out.println("\n\rTest is over. Closing!");

	}

}
