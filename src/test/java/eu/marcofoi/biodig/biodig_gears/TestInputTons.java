package eu.marcofoi.biodig.biodig_gears;

import static org.junit.Assert.assertEquals;

import java.net.URL;

import org.junit.Before;
import org.junit.Test;

import eu.marcofoi.biodig.biodig_gears.BioSystem;
import eu.marcofoi.biodig.biodig_gears.FermentationCalculusLogic;
import eu.marcofoi.biodig.biodig_gears.ICalculusLogic;
import eu.marcofoi.biodig.biodig_gears.InputTons;
import eu.marcofoi.biodig.biodig_gears.feeds.Feed;
import eu.marcofoi.biodig.biodig_gears.feeds.MaisFeed;

public class TestInputTons {
	
	InputTons inputTons;

	@Before
	public void setUp() throws Exception {
		
		// This is required to define the BioSystem life across the modeled year: a list of all subtractions in m3 (volumes)
		URL totalOutputFileURL = this.getClass().getResource("TOT_OUTPUT_M3.csv");
		// Path is relative to root as test run as being started from project root 
		URL urlFeed = this.getClass().getResource("./feeds/"+ Feed.TSS_FILENAME_PREFIX + Feed.SPACER + MaisFeed.FEED_NAME + ".csv");
		BioSystem bs = new BioSystem(2550.0, totalOutputFileURL/*.getPath()*/);

		//InputBag maizebag = new InputBag("maize");
		
		double startM3 = 27.0;
		Feed feedtype = new MaisFeed(urlFeed);
		
		int loadDay = 1;
		ICalculusLogic calculusLogic = new FermentationCalculusLogic(bs);
		
		inputTons = new InputTons(bs, startM3, feedtype, loadDay);
		
		bs.addInputTons(inputTons, calculusLogic);
		
		// Triggers execution of FermentationCalculusLogic for all InputTons in InputBag
		//bs.processInputs();
			
	}

	@Test
	public void testGetStartTons() {
		assertEquals(27.0, inputTons.getStartTons(), 0.0);
	}

	@Test
	public void testGetFeedtype() {
		assertEquals("MAIS", inputTons.getFeedtype().toString());
	}


}
