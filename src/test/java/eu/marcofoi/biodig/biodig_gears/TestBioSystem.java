package eu.marcofoi.biodig.biodig_gears;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.URL;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import eu.marcofoi.biodig.biodig_gears.feeds.Feed;
import eu.marcofoi.biodig.biodig_gears.feeds.LoiettoFeed;
import eu.marcofoi.biodig.biodig_gears.feeds.MaisFeed;
import eu.marcofoi.biodig.biodig_gears.io.CSVOutputWriterMultipleInputTonsWDifferentAgesPerDayBehaviour;
import eu.marcofoi.biodig.biodig_gears.io.CSVOutputWriterSameLifespanSumBehaviour;
import eu.marcofoi.biodig.biodig_gears.io.CSVOutputWriterSingleFreshInputTonsPerDayBehaviour;
import eu.marcofoi.biodig.biodig_gears.io.InputParser;
import eu.marcofoi.biodig.biodig_gears.io.XLSOutputWriterBehaviour;
import eu.marcofoi.biodig.biodig_gears.io.XLSXOutputWriterSUMValuesOfDayBehaviour;
import eu.marcofoi.biodig.biodig_gears.io.XLSXOutputWriterSUMandSINGLEInputPerDayBehaviour;

public class TestBioSystem {

	public static final boolean	keepExportFiles	= true;

	BioSystem					bs;
	String						testTotalOutputM3File;
	private URL					maizeLossFileURL;
	private URL					maizeGasProductionFileURL;
	private URL					maizeFeedsFileURL;
	private URL					loiettoFeedsFileURL;
	private URL					totalOutputFileURL;
	private URL					totalOutput;

	@Before
	public void setUp() throws Exception {

		// Pick test file from /src/test/resources/eu/marcofoi/biodig_gears/

		// This is needed to instantiate the MaisFeed instance
		// Path is relative to root as test run as being started from project root
		maizeLossFileURL = this.getClass().getResource("./feeds/TSS_MAIS.csv");

		// This is needed to instantiate the MaisFeed instance aiming to GAS loss computation
		// Path is relative to root as test run as being started from project root
		maizeGasProductionFileURL = this.getClass().getResource("./feeds/DAILY-MASS_to_GAS_MAIS.csv");

		// This is needed to simulate the daily maize volumes fed into the system:
		// a loop will produce 365 InputTons objects from it
		maizeFeedsFileURL = this.getClass().getResource("FEED_MAIS.csv");

		// This is required to define the BioSystem life across the modeled year: a list of all subtractions in m3
		// (volumes)
		totalOutputFileURL = this.getClass().getResource("TOT_OUTPUT_M3.csv");

		// Convert from URL to path
		// testTotalOutputM3File = totalOutput.getPath();

	}

	@Test
	/**
	 * Tests the creation of the BioSystem against a null value
	 */
	public void testCreateBioSystemOutputFileAsString() {
		bs = new BioSystem(2550.0, totalOutputFileURL);
		assertNotNull(bs);
	}

	@Test
	/**
	 * Tests the creation of the BioSystem against a null value
	 */
	public void testCreateBioSystemOutputFileAsURL() {
		bs = new BioSystem(2550.0, totalOutputFileURL);
		assertNotNull(bs);
	}

	/**
	 * Tests whether the input file containing the daily volumes extracted from
	 * the system is actually available to the BoSystem once it is provided in
	 * its constructor.
	 */
	@Test
	public void testCheckTotalOutput() {
		bs = new BioSystem(2550.0, totalOutputFileURL);
		double[] daylyOutputM3Array = bs.getDailyTotalOutputM3();
		assertNotNull(daylyOutputM3Array);
		assertEquals(40.98000, daylyOutputM3Array[0], 0.00005);
		assertEquals(38.43168, daylyOutputM3Array[daylyOutputM3Array.length - 1], 0.00005);
	}

	/**
	 * Tests whether the curve of dilution of the unit volume belonging to the
	 * BioSystem at its creation is correctly computed
	 */
	@Test
	public void testCreateDiluitionSequence() {
		bs = new BioSystem(2550.0, totalOutputFileURL);
		double[] dilSeq = bs.getDiluitionSequence();
		assertEquals(0.9945 / 2550.0, 0.00039, 0.0000005);
		assertEquals(3.39980691134979E-4, dilSeq[364], 0.0000005);
	}

	/**
	 * Tests the addition of a Feed (MaisFeed) to a BioSystem and check the
	 * number of feeds added.
	 */
	@Test
	public void testLoadInputs() {
		// Setup System
		bs = new BioSystem(2550.0, totalOutputFileURL);

		// Load inputs into system
		double[] feedsArray = InputParser.parseSingleColumnCSVFileToDoubleArray(maizeFeedsFileURL);

		Feed maizeFeedType = new MaisFeed(maizeLossFileURL);

		ICalculusLogic calculusLogic = new FermentationCalculusLogic(bs/* , maizeFeedType */);

		for (int i = 0; i < feedsArray.length; i++) {
			bs.addInputTons(new InputTons(bs, feedsArray[i], maizeFeedType, i + 1), calculusLogic);
		}

		assertEquals(365, bs.countInputs());

	}

	/**
	 * Tests the addition of a Feed (MaisFeed) to a BioSystem and check the
	 * number of feeds added, using the easy method addInputTonsFrom365RowsCSV()
	 */
	@Test
	public void testAddInputTonsFrom365RowsCSV() {
		// Setup System
		bs = new BioSystem(2550.0, totalOutputFileURL);

		// Load inputs into system
		double[] feedsArray = InputParser.parseSingleColumnCSVFileToDoubleArray(maizeFeedsFileURL);

		Feed maizeFeedType = new MaisFeed(maizeLossFileURL);

		ICalculusLogic calculusLogic = new FermentationCalculusLogic(bs/* , maizeFeedType */);

		bs.addInputTonsFrom365RowsCSV(maizeFeedsFileURL, maizeFeedType, calculusLogic);

		assertEquals(365, bs.countInputs());

	}

	/**
	 * Creates a MaisFeed, adds it o a BioSystem, launches the processing of
	 * the inputs and retrieves the output matrix.
	 */
	@Test
	public void testProcessInputs() {
		// Setup System
		bs = new BioSystem(2550.0, totalOutputFileURL);

		// Load inputs into system
		double[] feedsArray = InputParser.parseSingleColumnCSVFileToDoubleArray(maizeFeedsFileURL);

		Feed maizeFeedType = new MaisFeed(maizeLossFileURL);

		ICalculusLogic calculusLogic = new FermentationCalculusLogic(bs);

		for (int i = 0; i < feedsArray.length; i++) {
			bs.addInputTons(new InputTons(bs, feedsArray[i], maizeFeedType, i + 1), calculusLogic);
		}

		// Launch input processing
		bs.processInputs();

		// Grab computed data
		double[][][][][] fullSystemComputedMatrix = bs.getAllFeedsComputation();

		/*
		 * SAMPLE step-by-step computation double[][][] feedMAIZE =
		 * fullExport[0]; // Select maize double[][]
		 * computedArrayRESIDUAL_MASS_YEAR_ARRAY = feedMAIZE[0]; // Select
		 * RESIDUAL_MASS_YEAR_ARRAY double[] day_of_year1 =
		 * computedArrayRESIDUAL_MASS_YEAR_ARRAY[0]; // Select first row of
		 * matrix (day_of_year = 1)
		 */

		// STRUCTURE double[feedType][calculatedArry][day_of_year row][loadDay]

		// RESIDUAL_MASS_YEAR_ARRAY
		// first column: first and last row
		assertEquals(27.0, fullSystemComputedMatrix[0][0][0][0][0], 0.005);
		assertEquals(0.09424, fullSystemComputedMatrix[0][0][364][0][0], 0.00005);
		// second column: third and before-last row
		assertEquals(19.247878, fullSystemComputedMatrix[0][0][2][1][0], 0.000005);
		assertEquals(0.0914275, fullSystemComputedMatrix[0][0][363][1][0], 0.000005);
		// last column: first and last row
		assertEquals(Double.NaN, fullSystemComputedMatrix[0][0][0][364][0], 0.005);
		assertEquals(27.0, fullSystemComputedMatrix[0][0][364][364][0], 0.000005);

		// DAILY_OUTPUT_MASS_YEAR_ARRAY
		// first column: first and last row
		assertEquals(0.4315194, fullSystemComputedMatrix[0][1][0][0][0], 0.005);
		assertEquals(0.001231, fullSystemComputedMatrix[0][1][364][0][0], 0.000005);
		// second column: third and before-last row
		assertEquals(0.308448, fullSystemComputedMatrix[0][1][2][1][0], 0.0005);
		assertEquals(0.00124311, fullSystemComputedMatrix[0][1][363][1][0], 0.000005);
		// last column: first and last row
		assertEquals(Double.NaN, fullSystemComputedMatrix[0][1][0][364][0], 0.005);
		assertEquals(0.433905882, fullSystemComputedMatrix[0][1][364][364][0], 0.000005);

		// TSS_LOST_YEAR_ARRAY;
		// first column: first and last row
		assertEquals(6.185987766, fullSystemComputedMatrix[0][2][0][0][0], 0.00000005);
		assertEquals(1E-167, fullSystemComputedMatrix[0][2][364][0][0], 0.5E-167);
		// second column: third and before-last row
		assertEquals(3.620487417, fullSystemComputedMatrix[0][2][2][1][0], 0.00000005);
		assertEquals(4.4582E-166, fullSystemComputedMatrix[0][2][363][1][0], 0.005E-166);
		// last column: first and last row
		assertEquals(Double.NaN, fullSystemComputedMatrix[0][2][0][364][0], 0.005);
		assertEquals(6.185987766, fullSystemComputedMatrix[0][2][364][364][0], 0.000005);

	}

	/**
	 * Creates a MaisFeed, adds it o a BioSystem, launches the processing of
	 * the inputs and retrieves the output matrix. Then picks the resulting
	 * matrix and dumps it in several formats:
	 * <ul>
	 * <li>CSV</li>
	 * <li>XLS</li>
	 * <li>XLSX</li>
	 * </ul>
	 */
	@Ignore
	public void testExportComputedData() {

		// Force clean output dir
		cleanMaizeFilesNew(BioSystem.defaultBioSystemName);

		// Setup System
		bs = new BioSystem(2550.0, totalOutputFileURL);

		// Load inputs into system
		double[] feedsArray = InputParser.parseSingleColumnCSVFileToDoubleArray(maizeFeedsFileURL);

		final Feed maizeFeedType = new MaisFeed(maizeLossFileURL);

		ICalculusLogic calculusLogic = new FermentationCalculusLogic(bs/* , maizeFeedType */);

		for (int i = 0; i < feedsArray.length; i++) {
			bs.addInputTons(new InputTons(bs, feedsArray[i], maizeFeedType, i + 1), calculusLogic);
		}

		// Launch input processing
		bs.processInputs();

		// Dump some results in CSV
		bs.exportAllFeedComputation(new CSVOutputWriterSingleFreshInputTonsPerDayBehaviour());

		// Dump some results: XLS is limited to 256 columns!
		bs.exportAllFeedComputation(new XLSOutputWriterBehaviour());

		// Dump some results in XLSX
		bs.exportAllFeedComputation(new XLSXOutputWriterSUMandSINGLEInputPerDayBehaviour());

		// Count exported files
		File path = new File("./" + BioSystemCLI.OUTPUT_FOLDER);
		File[] files = null;
		if (path.isDirectory()) {
			FileFilter ff = new FileFilter() {

				public boolean accept(File pathname) {
					if (pathname.getName().startsWith(bs.getName() + "_" + maizeFeedType.getFeedName())) {
						return true;
					} else {
						return false;
					}
				}
			};
			files = path.listFiles(ff);
		}

		assertEquals(9, files.length);
	}

	/**
	 * Creates a MaisFeed, adds it to a BioSystem, launches the processing of
	 * the inputs and retrieves the output matrix. Then picks the resulting
	 * matrix and dumps it in several formats:
	 * <ul>
	 * <li>CSV</li>
	 * <li>XLS</li>
	 * <li>XLSX</li>
	 * </ul>
	 */
	@Ignore
	public void testExportComputedGASData() {

		// Force clean output dir
		cleanMaizeFilesNew(BioSystem.defaultBioSystemName);

		// Setup System
		bs = new BioSystem(2550.0, totalOutputFileURL);

		// Load inputs into system
		double[] feedsArray = InputParser.parseSingleColumnCSVFileToDoubleArray(maizeFeedsFileURL);

		final Feed maizeFeedType = new MaisFeed(); // Use constructor relying on default resources

		ICalculusLogic calculusLogic = new FermentationAndGasLossCalculusLogic(bs/* , maizeFeedType */);

		/*
		 * for (int i = 0; i < feedsArray.length; i++) {
		 * bs.addInputTons(new InputTons(bs, feedsArray[i], maizeFeedType, i + 1), calculusLogic);
		 * }
		 */

		bs.addInputTonsFrom365RowsCSV(maizeFeedsFileURL, maizeFeedType, calculusLogic);

		// Launch input processing
		bs.processInputs();

		// Dump some results in CSV
		bs.exportAllFeedComputation(new CSVOutputWriterSingleFreshInputTonsPerDayBehaviour());

		// Dump some results: XLS is limited to 256 columns!
		bs.exportAllFeedComputation(new XLSOutputWriterBehaviour());

		// Dump some results in XLSX
		bs.exportAllFeedComputation(new XLSXOutputWriterSUMandSINGLEInputPerDayBehaviour());

		// Count exported files
		File path = new File("./" + BioSystemCLI.OUTPUT_FOLDER);
		File[] files = null;
		if (path.isDirectory()) {
			FileFilter ff = new FileFilter() {

				public boolean accept(File pathname) {
					if (pathname.getName().startsWith(bs.getName() + "_" + maizeFeedType.getFeedName())) {
						return true;
					} else {
						return false;
					}
				}
			};
			files = path.listFiles(ff);
		}

		assertEquals(12, files.length);
	}

	/**
	 * Creates a MaisFeed, adds it to a BioSystem, launches the processing of
	 * the inputs and retrieves the output matrix.
	 * .... fix description of test ....
	 * Then picks the resulting
	 * matrix and dumps it in several formats:
	 * <ul>
	 * <li>CSV</li>
	 * <li>XLS</li>
	 * <li>XLSX</li>
	 * </ul>
	 */
	@Ignore
	public void testTwoPools() {

		// Force clean output dir
		cleanMaizeFilesNew(BioSystem.defaultBioSystemName);

		// Setup System
		bs = new BioSystem(2550.0, totalOutputFileURL);

		// Create feed using constructor relying on default resources
		Feed maizeFeedType = new MaisFeed();

		ICalculusLogic calculusLogic = new FermentationAndGasLossCalculusLogic(bs);

		// Load inputs into system
		bs.addInputTonsFrom365RowsCSV(maizeFeedsFileURL, maizeFeedType, calculusLogic);

		// Launch input processing
		bs.processInputs();

		// Get computed data
		// double[][][][][] data1 = bs.getAllFeedsComputation();
		// double[][][][] data2 = bs.getSingleFeedComputation(maizeFeedType);
		// double[][][][] data3 = bs.getSingleFeedSingleArrayComputation(maizeFeedType,
		// FermentationAndGasArrays.DAILY_OUTPUT_MASS_YEAR_ARRAY);
		double[][][][] data = bs.getOutputFeedsMatrix(maizeFeedType);

		// Create SECOND STAGE
		BioSystem bs2ndstage = new BioSystem(2550.0, totalOutputFileURL);

		bs2ndstage.addInputTonsFromCaclulatedArrayYearMatrix(data, maizeFeedType, calculusLogic);

		// Destroy FIRST STAGE to save memory
		bs = null;

		assertEquals(66795, bs2ndstage.countInputs());

		// Launch input processing
		bs2ndstage.processInputs();

		// Dump some results in CSV
		bs2ndstage.exportAllFeedComputation(new CSVOutputWriterMultipleInputTonsWDifferentAgesPerDayBehaviour());

		// Dump some results: XLS is limited to 256 columns!
		// bs.exportAllFeedComputation(new XLSOutputWriterBehaviour());

		// Dump some results in XLSX
		// bs.exportAllFeedComputation(new XLSXOutputWriterSUMandSINGLEInputPerDayBehaviour());

	}

	/**
	 * Creates TWO FEEDS, one MaisFeed and one LoiettoFeed, adds them to a BioSystem, launches the processing of
	 * the inputs and retrieves the output matrix. Then picks the resulting
	 * matrix and dumps it in several formats:
	 * <ul>
	 * <li>CSV</li>
	 * <li>XLS</li>
	 * <li>XLSX</li>
	 * </ul>
	 */
	@Ignore
	public void testTwoFeedOneStageTypesExportComputedGASData() {

		// Force clean output dir
		cleanAllFeedsFilesNew(BioSystem.defaultBioSystemName);

		// Setup System
		bs = new BioSystem(2550.0, totalOutputFileURL);

		// Load inputs into system
		double[] maizeFeedsArray = InputParser.parseSingleColumnCSVFileToDoubleArray(maizeFeedsFileURL);

		loiettoFeedsFileURL = this.getClass().getResource("FEED_LOIETTO.csv");
		// Load inputs into system
		double[] loiettoFeedsArray = InputParser.parseSingleColumnCSVFileToDoubleArray(loiettoFeedsFileURL);

		final Feed maizeFeedType = new MaisFeed(); // Use constructor relying on default resources
		final Feed loiettoFeedType = new LoiettoFeed(); // Use constructor relying on default resources

		ICalculusLogic calculusLogic = new FermentationAndGasLossCalculusLogic(bs/* , maizeFeedType */);

		bs.addInputTonsFrom365RowsCSV(maizeFeedsFileURL, maizeFeedType, calculusLogic);
		bs.addInputTonsFrom365RowsCSV(loiettoFeedsFileURL, loiettoFeedType, calculusLogic);

		// Launch input processing
		bs.processInputs();

		// Dump some results in CSV
		bs.exportAllFeedComputation(new CSVOutputWriterSingleFreshInputTonsPerDayBehaviour());

		// Dump some results: XLS is limited to 256 columns!
		// bs.exportAllFeedComputation(new XLSOutputWriterBehaviour());

		// Dump some results in XLSX
		// bs.exportAllFeedComputation(new XLSXOutputWriterSUMandSINGLEInputPerDayBehaviour());

		// Count exported files
		File path = new File("./" + BioSystemCLI.OUTPUT_FOLDER);
		File[] files = null;
		if (path.isDirectory()) {
			FileFilter ff = new FileFilter() {

				public boolean accept(File pathname) {
					if (pathname.getName().startsWith(bs.getName() + "_" + maizeFeedType.getFeedName())) {
						return true;
					} else {
						return false;
					}
				}
			};
			files = path.listFiles(ff);
		}

		assertEquals(4, files.length);
		
		if (path.isDirectory()) {
			FileFilter ff = new FileFilter() {

				public boolean accept(File pathname) {
					if (pathname.getName().startsWith(bs.getName() + "_" + loiettoFeedType.getFeedName())) {
						return true;
					} else {
						return false;
					}
				}
			};
			files = path.listFiles(ff);
		}

		assertEquals(4, files.length);
		
		
	}
	

	/**
	 * Creates TWO FEEDS, one MaisFeed and one LoiettoFeed, adds them to a BioSystem, launches the processing of
	 * the inputs and retrieves the output matrix. Then picks the resulting
	 * matrix and dumps it in several formats:
	 * <ul>
	 * <li>CSV</li>
	 * <li>XLS</li>
	 * <li>XLSX</li>
	 * </ul>
	 */
	@Test
	public void testOneFeedTwoStagesTypesExportComputedGASData() {
		
		String secondStageBSname = "BS2s";

		// Force clean output dir
		cleanAllFeedsFilesNew(BioSystem.defaultBioSystemName);
		cleanAllFeedsFilesNew(secondStageBSname);

		// Setup System
		bs = new BioSystem(2550.0, totalOutputFileURL);

		// Load inputs into system
		double[] maizeFeedsArray = InputParser.parseSingleColumnCSVFileToDoubleArray(maizeFeedsFileURL);

		//loiettoFeedsFileURL = this.getClass().getResource("FEED_LOIETTO.csv");
		// Load inputs into system
		//double[] loiettoFeedsArray = InputParser.parseSingleColumnCSVFileToDoubleArray(loiettoFeedsFileURL);

		final Feed maizeFeedType = new MaisFeed(); // Use constructor relying on default resources
		//final Feed loiettoFeedType = new LoiettoFeed(); // Use constructor relying on default resources

		ICalculusLogic calculusLogic = new FermentationAndGasLossCalculusLogic(bs/* , maizeFeedType */);

		bs.addInputTonsFrom365RowsCSV(maizeFeedsFileURL, maizeFeedType, calculusLogic);
		//bs.addInputTonsFrom365RowsCSV(loiettoFeedsFileURL, loiettoFeedType, calculusLogic);

		// Launch input processing
		bs.processInputs();

		// Dump some results in CSV
		//bs.exportAllFeedComputation(new CSVOutputWriterSingleFreshInputTonsPerDayBehaviour());

		// Dump some results: XLS is limited to 256 columns!
		// bs.exportAllFeedComputation(new XLSOutputWriterBehaviour());

		// Dump some results in XLSX
		// bs.exportAllFeedComputation(new XLSXOutputWriterSUMandSINGLEInputPerDayBehaviour());

		List<Feed> feedList = new ArrayList<Feed>();
		feedList.add(maizeFeedType);
		//feedList.add(loiettoFeedType);

		// Create SECOND STAGE
		final BioSystem bs2ndstage = new BioSystem(2550.0, totalOutputFileURL);

		// Set a not-default name (would be "BS") to help tracking output in filnames.
		bs2ndstage.setName(secondStageBSname);
		
		// Cycle in all processed feeds
		for (Feed feed : feedList) {

			double[][][][] data = bs.getOutputFeedsMatrix(feed);

			// Add inputTons from output of previous stage
			bs2ndstage.addInputTonsFromCaclulatedArrayYearMatrix(data, feed, calculusLogic);
		}
		
		// Destroy FIRST STAGE to save memory
		bs = null;

		// Launch input processing
		bs2ndstage.processInputs();
		
		// Export handy Sums in .CSV
		// REMOVED: export is too slow and Sums are already exported in Excel. See below.
		//bs2ndstage.exportAllFeedComputation(new CSVOutputWriterDailySumBehaviour());
		
		// Export handy Sums in Excel
		// TODO Restore this dump
		//bs2ndstage.exportAllFeedComputation(new XLSXOutputWriterSUMValuesOfDayBehaviour());
		
		// Export full result in .CSV (big files!!)
		// TODO Restore this dump
		bs2ndstage.exportAllFeedComputation(new CSVOutputWriterMultipleInputTonsWDifferentAgesPerDayBehaviour());
		
		// Export Sums of same-lifespan contributes in .CSV
		bs2ndstage.exportAllFeedComputation(new CSVOutputWriterSameLifespanSumBehaviour());
		
		
		// Count exported files
		File path = new File("./" + BioSystemCLI.OUTPUT_FOLDER);
		File[] files = null;
		if (path.isDirectory()) {
			FileFilter ff = new FileFilter() {

				public boolean accept(File pathname) {
					if (pathname.getName().startsWith(bs2ndstage.getName() + "_" + maizeFeedType.getFeedName())) {
						return true;
					} else {
						return false;
					}
				}
			};
			files = path.listFiles(ff);
		}

		assertEquals(12, files.length);
		
		/*
		if (path.isDirectory()) {
			FileFilter ff = new FileFilter() {

				public boolean accept(File pathname) {
					if (pathname.getName().startsWith(bs2ndstage.getName() + "_" + loiettoFeedType.getFeedName())) {
						return true;
					} else {
						return false;
					}
				}
			};
			files = path.listFiles(ff);
		}
		*/

		assertEquals(8, files.length);
		
		
	}
	

	/**
	 * Creates TWO FEEDS, one MaisFeed and one LoiettoFeed, adds them to a BioSystem, launches the processing of
	 * the inputs and retrieves the output matrix. Then picks the resulting
	 * matrix and dumps it in several formats:
	 * <ul>
	 * <li>CSV</li>
	 * <li>XLS</li>
	 * <li>XLSX</li>
	 * </ul>
	 */
	@Ignore
	public void testTwoFeedTwoStagesTypesExportComputedGASData() {
		
		String secondStageBSname = "BS2s";

		// Force clean output dir
		cleanAllFeedsFilesNew(BioSystem.defaultBioSystemName);
		cleanAllFeedsFilesNew(secondStageBSname);

		// Setup System
		bs = new BioSystem(2550.0, totalOutputFileURL);

		// Load inputs into system
		double[] maizeFeedsArray = InputParser.parseSingleColumnCSVFileToDoubleArray(maizeFeedsFileURL);

		loiettoFeedsFileURL = this.getClass().getResource("FEED_LOIETTO.csv");
		// Load inputs into system
		double[] loiettoFeedsArray = InputParser.parseSingleColumnCSVFileToDoubleArray(loiettoFeedsFileURL);

		final Feed maizeFeedType = new MaisFeed(); // Use constructor relying on default resources
		final Feed loiettoFeedType = new LoiettoFeed(); // Use constructor relying on default resources

		ICalculusLogic calculusLogic = new FermentationAndGasLossCalculusLogic(bs/* , maizeFeedType */);

		bs.addInputTonsFrom365RowsCSV(maizeFeedsFileURL, maizeFeedType, calculusLogic);
		bs.addInputTonsFrom365RowsCSV(loiettoFeedsFileURL, loiettoFeedType, calculusLogic);

		// Launch input processing
		bs.processInputs();

		// Dump some results in CSV
		//bs.exportAllFeedComputation(new CSVOutputWriterSingleFreshInputTonsPerDayBehaviour());

		// Dump some results: XLS is limited to 256 columns!
		// bs.exportAllFeedComputation(new XLSOutputWriterBehaviour());

		// Dump some results in XLSX
		// bs.exportAllFeedComputation(new XLSXOutputWriterSUMandSINGLEInputPerDayBehaviour());

		List<Feed> feedList = new ArrayList<Feed>();
		feedList.add(maizeFeedType);
		feedList.add(loiettoFeedType);

		// Create SECOND STAGE
		final BioSystem bs2ndstage = new BioSystem(2550.0, totalOutputFileURL);

		// Set a not-default name (would be "BS") to help tracking output in filnames.
		bs2ndstage.setName(secondStageBSname);
		
		// Cycle in all processed feeds
		for (Feed feed : feedList) {

			double[][][][] data = bs.getOutputFeedsMatrix(feed);

			// Add inputTons from output of previous stage
			bs2ndstage.addInputTonsFromCaclulatedArrayYearMatrix(data, feed, calculusLogic);
		}
		
		// Destroy FIRST STAGE to save memory
		bs = null;

		// Launch input processing
		bs2ndstage.processInputs();
		
		// Export handy Sums in .CSV
		// REMOVED: export is too slow and Sums are altredy exported in Excel. See below.
		//bs2ndstage.exportAllFeedComputation(new CSVOutputWriterDailySumBehaviour());
		
		// Export handy Sums in Excel
		bs2ndstage.exportAllFeedComputation(new XLSXOutputWriterSUMValuesOfDayBehaviour());
		
		// Export full result in .CSV (big files!!)
		bs2ndstage.exportAllFeedComputation(new CSVOutputWriterMultipleInputTonsWDifferentAgesPerDayBehaviour());
		
		// Export Sums of same-lifespan contributes in .CSV
		bs2ndstage.exportAllFeedComputation(new CSVOutputWriterSameLifespanSumBehaviour());
		
		
		// Count exported files
		File path = new File("./" + BioSystemCLI.OUTPUT_FOLDER);
		File[] files = null;
		if (path.isDirectory()) {
			FileFilter ff = new FileFilter() {

				public boolean accept(File pathname) {
					if (pathname.getName().startsWith(bs2ndstage.getName() + "_" + maizeFeedType.getFeedName())) {
						return true;
					} else {
						return false;
					}
				}
			};
			files = path.listFiles(ff);
		}

		assertEquals(8, files.length);
		
		if (path.isDirectory()) {
			FileFilter ff = new FileFilter() {

				public boolean accept(File pathname) {
					if (pathname.getName().startsWith(bs2ndstage.getName() + "_" + loiettoFeedType.getFeedName())) {
						return true;
					} else {
						return false;
					}
				}
			};
			files = path.listFiles(ff);
		}

		assertEquals(8, files.length);
		
		
	}

	// ### END OF TESTs ############################################

	@After
	public void cleanUp() {

		if (!keepExportFiles) {
			cleanMaizeFilesNew(BioSystem.defaultBioSystemName);
		}
	}

	/**
	 * This method cleans all files created during test and whose name starts
	 * with "maize". It relies on JRE 1.6 but seems not to clean all files
	 * whereas {@link #cleanMaizeFilesNew()} has proved to be effective but uses
	 * JRE 1.7
	 */
	@Deprecated
	private void cleanMaizeFiles() {

		File path = new File("./" + BioSystemCLI.OUTPUT_FOLDER);
		if (path.isDirectory()) {
			FileFilter ff = new FileFilter() {

				public boolean accept(File pathname) {
					if (pathname.getName().startsWith(bs.getName() + "_" + MaisFeed.FEED_NAME)) {
						return true;
					} else {
						return false;
					}
				}
			};
			File[] files = path.listFiles(ff);
			for (int i = 0; i < files.length; i++) {

				files[0].setWritable(true);
				// if(Files.delete(path))
				if (!files[0].delete()) {
					files[0].deleteOnExit();
				}
			}
		}
	}

	/**
	 * This method cleans all files created during test and whose name starts
	 * with "maize". It relies on JRE 1.7 and its java.nio.file.* package.
	 */
	private void cleanMaizeFilesNew(final String bioSystemName) {
		try {

			DirectoryStream.Filter<Path> filter = new DirectoryStream.Filter<Path>() {
				public boolean accept(Path file) throws java.io.IOException {
					if (!Files.isDirectory(file, LinkOption.NOFOLLOW_LINKS)) {
						Path filename = file.getFileName();
						boolean toBeDeleted = filename.toString().startsWith(bioSystemName + "_" + MaisFeed.FEED_NAME);
						return toBeDeleted;
					} else {
						return false;
					}
				};
			};

			Path dir = Paths.get("./" + BioSystemCLI.OUTPUT_FOLDER);

			DirectoryStream<Path> stream = Files.newDirectoryStream(dir, filter);
			Iterator<Path> i = stream.iterator();
			while (i.hasNext()) {
				Path path = i.next();
				Files.delete(path);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	

	/**
	 * This method cleans all files created during test and whose name starts
	 * with "maize". It relies on JRE 1.7 and its java.nio.file.* package.
	 */
	private void cleanAllFeedsFilesNew(final String bioSystemName) {
		try {

			DirectoryStream.Filter<Path> filter = new DirectoryStream.Filter<Path>() {
				public boolean accept(Path file) throws java.io.IOException {
					if (!Files.isDirectory(file, LinkOption.NOFOLLOW_LINKS)) {
						Path filename = file.getFileName();
						boolean toBeDeleted = filename.toString().startsWith(bioSystemName + "_");
						return toBeDeleted;
					} else {
						return false;
					}
				};
			};

			Path dir = Paths.get("./" + BioSystemCLI.OUTPUT_FOLDER);

			DirectoryStream<Path> stream = Files.newDirectoryStream(dir, filter);
			Iterator<Path> i = stream.iterator();
			while (i.hasNext()) {
				Path path = i.next();
				Files.delete(path);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
